import { describe, expect, it } from "@jest/globals";
import { parseProgram } from "../../parser/parser";

type ParserTest = {
  name: string;
  input: string;
};

describe(`Parser: functions`, () => {
  const testCases: ParserTest[] = [
    {
      name: "Simple delegate - zero arguments",
      input: "const f = -> 2 + 2;",
    },
    {
      name: "Simple delegate - single argument",
      input: "const f = x:int -> x * x;",
    },
    {
      name: "Simple delegate - multiple arguments",
      input: "const f = x:int, y:int -> x * y;",
    },
    {
      name: "Local function definition",
      input: "const add = x:int, y:int -> x + y;",
    },
    {
      name: "Local function definition - with parenthesis",
      input: "const add = x:int, y:int -> x + y;",
    },

    {
      name: "Full body delegate - zero arguments",
      input: "const f = -> { 2 + 2; };",
    },
    {
      name: "Full body delegate - single argument",
      input: "const f = x: int -> { x * x; };",
    },
    {
      name: "Full body delegate - multiple arguments",
      input: "const f = x: int, y: int -> { x * y; };",
    },
    {
      name: "Function call",
      input: `
        const multiply = x: int, y: int -> x * y;
        multiply(2,5);
      `,
    },
    {
      name: "Nested function call",
      input: `
        const multiply = x: int, y: int -> x * y;
        const add = x: int, y: int -> x + y;
        multiply(2,add(2, 5));
      `,
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      expect(parseProgram(test.input)).toMatchSnapshot();
    });
  });
});
