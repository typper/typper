import { describe, expect, it } from "@jest/globals";
import { parseProgram } from "../../parser/parser";

type ParserTest = {
  name: string;
  input: string;
};

describe(`Parser: blocks`, () => {
  const testCases: ParserTest[] = [
    {
      name: "Regular block",
      input: "{ const x = 1; }",
    },
    {
      name: "Nested block",
      input: "const x = 0; { x = 2; { x = 3; } }",
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      expect(parseProgram(test.input)).toMatchSnapshot();
    });
  });
});
