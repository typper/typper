import { describe, expect, it } from "@jest/globals";
import { parseProgram } from "../../parser/parser";

type ParserTest = {
  name: string;
  input: string;
};

describe(`Parser: objects`, () => {
  const testCases: ParserTest[] = [
    {
      name: "Anonymous object",
      input: "{ name: '' };",
    },
    {
      name: "Object literal type",
      input: "const x: { name: string } = { name: '' };",
    },
    {
      name: "Nested objects",
      input:
        "const x: { name: string, data: { age: int, size: int } } = { name: '', data: { age: 2, size: 12 } };",
    },
    {
      name: "Method property",
      input: "const x: { getValue: -> int } = { getValue: -> 2 };",
    },
    {
      name: "Property access",
      input:
        "const x: { getValue: -> int } = { getValue: -> 2 }; x.getValue();",
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      expect(parseProgram(test.input)).toMatchSnapshot();
    });
  });
});
