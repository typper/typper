import { describe, expect, it } from "@jest/globals";
import { parseExpression } from "../../parser/parser";

type ParserTest = {
  name: string;
  input: string;
};

describe(`Parser: basic arithmetic`, () => {
  const testCases: ParserTest[] = [
    {
      name: "Standalone constant",
      input: "1",
    },
    {
      name: "Simple sum",
      input: "1 + 2",
    },
    {
      name: "Boolean true",
      input: "true",
    },
    {
      name: "Boolean false",
      input: "false",
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      expect(parseExpression(test.input)).toMatchSnapshot();
    });
  });
});
