import { beforeEach, describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";
import { ExecutionScope } from "../../runtime/scope";

const globalScope = new ExecutionScope();
const testCases = [
  {
    name: "Basic if",
    input: `
        var result = 0;
        if (true) {
            result = 1; 
        }
      `,
    result: 1,
  },
  {
    name: "If / else - true condition",
    input: `
      var result = 0;
      if (true) {
          result = 1;
      } else {
          result = 2;
      }
    `,
    result: 1,
  },
  {
    name: "If / else - false condition",
    input: `
      var result = 0;
      if (false) {
          result = 1;
      } else {
          result = 2;
      }
    `,
    result: 2,
  },
  {
    name: "If / else if / else - first condition true",
    input: `
      var result = 0;
      if (true) {
          result = 1;
      } else if (false) {
          result = 2;
      } else {
          result = 3;
      }
    `,
    result: 1,
  },
  {
    name: "If / else if / else - second condition true",
    input: `
      var result = 0;
      if (false) {
          result = 1;
      } else if (true) {
          result = 2;
      } else {
          result = 3;
      }
    `,
    result: 2,
  },
  {
    name: "If / else if / else - both conditions false",
    input: `
      var result = 0;
      if (false) {
          result = 1;
      } else if (false) {
          result = 2;
      } else {
          result = 3;
      }
    `,
    result: 3,
  },
  {
    name: "Nested ifs",
    input: `
      var result = 0;
      if (false) {
          result = 1;
      } else if (false) {
          result = 2;
      } else {
        if (false) {
          result = 3;
        } else if (true) {
          result = 4;
        } else {
          result = 5;
        }
      }
    `,
    result: 4,
  },
];

describe(`Runtime: conditions`, () => {
  beforeEach(() => globalScope.clear());
  testCases.forEach((test) => {
    it(`${test.name}`, () => {
      executeProgram(test.input, globalScope);
      expect(globalScope.getVariable("result").value).toEqual(test.result);
    });
  });
});
