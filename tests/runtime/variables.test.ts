import { beforeEach, describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";
import { deepDereference, ExecutionScope } from "../../runtime/scope";

describe(`Runtime: variable declaration and assignment`, () => {
  const globalScope = new ExecutionScope();
  beforeEach(() => {
    globalScope.clear();
  });
  const resultTestCases = [
    {
      name: "should save variable state",
      input: `
        const result = 12;
      `,
      result: 12,
    },
    {
      name: "evaluates expression",
      input: `
        const result = 12 + 2;
      `,
      result: 14,
    },
    {
      name: "allows overriding variables",
      input: `
        var result = 12 + 2; result = 3;
      `,
      result: 3,
    },
    {
      name: "cannot redeclare const variable",
      input: `
        var result = 12 + 2; result = 3;
      `,
      result: 3,
    },
    {
      name: "replaces value of variable in expression",
      input: `
        const b = 12 + 2; 
        const result = b * 20 - (b + 3) + 3;
      `,
      result: 266,
    },
  ];

  resultTestCases.forEach((test) => {
    it(`${test.name}`, () => {
      executeProgram(test.input, globalScope);
      expect(deepDereference(globalScope.getVariable("result"))).toEqual(
        test.result,
      );
    });
  });

  const throwTestCases = [
    {
      name: "cannot redeclare const variable",
      input: `
        const c = 3; const c = 12;
      `,
    },
    {
      name: "cannot redeclare var variable",
      input: `
        var c = 3;var c = 12;
      `,
    },
    {
      name: "cannot override const variable",
      input: `
        const c = 3; 
        c = 12;
      `,
    },
    {
      name: "cannot self-reference while declaring variable",
      input: `
        const c = c + 2; 
      `,
    },
  ];

  throwTestCases.forEach((test) => {
    it(`${test.name}`, () => {
      expect(() => {
        const result = executeProgram(test.input, globalScope);
        console.log(globalScope.getVariable("c"));
      }).toThrow();
    });
  });
});
