import { describe, expect, it } from "@jest/globals";
import { executeExpression } from "../../runtime/run";

describe(`Runtime: basic arithmetic`, () => {
  const testCases = [
    { name: "Positive integer", input: "1", result: 1 },
    { name: "Negative integer", input: "-1", result: -1 },
    { name: "Positive decimal", input: "1.23", result: 1.23 },
    { name: "Negative decimal", input: "-1.23", result: -1.23 },
    { name: "Boolean true", input: "true", result: true },
    { name: "Boolean false", input: "false", result: false },
    { name: "String", input: "'x'", result: "x" },
    { name: "String template", input: "`x`", result: "x" },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      expect(executeExpression(test.input)).toEqual(test.result);
    });
  });
});
