import { describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";

describe(`Runtime: function calls`, () => {
  const testCases = [
    {
      name: "Function call",
      input: "const multiply = x:int, y:int -> x * y; multiply(2,10);",
      result: 20,
    },
    {
      name: "Nested function calls",
      input: `
        const multiply = x:int, y:int -> x * y; 
        const add = x:int, y:int -> x + y; 
        multiply(add(1,2),10);
      `,
      result: 30,
    },
    {
      name: "Full body method without args",
      input: `const execute = -> { return 2+3; }; execute();`,
      result: 5,
    },
    {
      name: "Early return",
      input: `
        const execute = -> {
          return 2;
          return 3;
          4;
        };
        execute();
      `,
      result: 2,
    },
    {
      name: "Two exp",
      input: `
        const execute = -> {
          return 2;
          return 3;
          4;
        };
        execute();
      `,
      result: 2,
    },
    {
      name: "Early return - nested blocks",
      input: `
        const execute = -> {
          if (true) {
            return 2;
          }
          return 3;
          4;
        };
        execute();
      `,
      result: 2,
    },
  ];
  testCases.forEach(({ name, input, result }) => {
    it(name, () => {
      expect(executeProgram(input)).toEqual(result);
    });
  });
});
