import { beforeEach, describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";
import { deepDereference, ExecutionScope } from "../../runtime/scope";

describe(`Runtime: strings`, () => {
  const globalScope = new ExecutionScope();
  beforeEach(() => {
    globalScope.clear();
  });
  const testCases = [
    { name: "String concat", input: "const result = 'a' + 'b';", result: "ab" },
    {
      name: "String template",
      input: `
            const x = \`X\`; 
            const result = \`a{x}b\`;
          `,
      result: "aXb",
    },
    {
      name: "String template - argument first",
      input: `
            const x = \`X\`; 
            const num = 1;
            const result = \`{num}a{x}b\`;
          `,
      result: "1aXb",
    },
    {
      name: "String template - string part first",
      input: `
            const x = \`X\`; 
            const num = 1;
            const result = \`x{num * 2}a{x}b\`;
          `,
      result: "x2aXb",
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}`, () => {
      executeProgram(test.input, globalScope);
      expect(deepDereference(globalScope.getVariable("result"))).toEqual(
        test.result,
      );
    });
  });
});
