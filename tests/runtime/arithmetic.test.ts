import { describe, expect, it } from "@jest/globals";
import { executeExpression } from "../../runtime/run";

describe(`Runtime: basic arithmetic`, () => {
  const testCases = [
    { name: "Standalone constant", input: "1", result: 1 },
    { name: "Constant with sign", input: "+1.5", result: 1.5 },
    { name: "Constant with sign", input: "-0.5", result: -0.5 },
    { name: "Simple addition", input: "1 + 2", result: 3 },
    { name: "Simple subtraction", input: "1 - 2", result: -1 },
    { name: "Simple multiplication", input: "1 * 2 * 3", result: 6 },
    { name: "Simple division", input: "1 / 2", result: 0.5 },
    { name: "Evaluation order", input: "1 + 2 * 3 + 4", result: 11 },
    { name: "Parenthesis", input: "(1 + 2) * (3 + 4)", result: 21 },
    {
      name: "Nested parenthesis",
      input: "(1 + 2 * (1 + 1)) * (3 + 4)",
      result: 35,
    },
    { name: "Signs in complex expressions", input: "1.2--3.4", result: 4.6 },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      expect(executeExpression(test.input)).toEqual(test.result);
    });
  });
});
