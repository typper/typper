import { beforeEach, describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";
import { deepDereference, ExecutionScope } from "../../runtime/scope";

const globalScope = new ExecutionScope();

const testCases = [
  {
    name: "fib(3)",
    input: `
        const fib = a: int -> {
            var result = 0;
            if (a == 0) {
                result = 0;
            } else if (a == 1) { 
                result = 1;
            } else {
                result = fib(a - 1) + fib(a - 2);
            }
            result;            
        };
        const x = fib(3);
      `,
    result: 2,
  },
  {
    name: "fib(5)",
    input: `
        const fib = a: int -> {
            var result = 0;
            if (a == 0) {
                result = 0;
            } else if (a == 1) { 
                result = 1;
            } else {
                result = fib(a - 1) + fib(a - 2);
            }
            result;            
        };
        const x = fib(6);
      `,
    result: 8,
  },
];

describe(`Runtime: recursion`, () => {
  beforeEach(() => globalScope.clear());
  testCases.forEach((test) => {
    it(`${test.name}`, () => {
      executeProgram(test.input, globalScope);
      expect(deepDereference(globalScope.getVariable("x"))).toEqual(
        test.result,
      );
    });
  });
});
