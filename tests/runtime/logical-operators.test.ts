import { describe, expect, it } from "@jest/globals";
import { TokenKind } from "../../parser/lexer";
import { executeExpression } from "../../runtime/run";

const testCases = {
  [TokenKind.And]: [
    { input: "true && true", result: true },
    { input: "true && false", result: false },
    { input: "false && true", result: false },
    { input: "false && false", result: false },
  ],
  [TokenKind.Or]: [
    { input: "true || true", result: true },
    { input: "true || false", result: true },
    { input: "false || true", result: true },
    { input: "false || false", result: false },
  ],
  [TokenKind.Equal]: [
    { input: "true == true", result: true },
    { input: "true == false", result: false },
    { input: "1 == 1", result: true },
    { input: "1 == 2", result: false },
  ],
  [TokenKind.Greater]: [
    { input: "1 > 2", result: false },
    { input: "2 > 1", result: true },
    { input: "1 > 1", result: false },
  ],
  [TokenKind.GreaterEqual]: [
    { input: "1 >= 2", result: false },
    { input: "2 >= 1", result: true },
    { input: "1 >= 1", result: true },
  ],
  [TokenKind.Less]: [
    { input: "1 < 2", result: true },
    { input: "2 < 1", result: false },
    { input: "1 < 1", result: false },
  ],
  [TokenKind.LessEqual]: [
    { input: "1 <= 2", result: true },
    { input: "2 <= 1", result: false },
    { input: "1 <= 1", result: true },
  ],
  [TokenKind.NotEqual]: [
    { input: "1 != 2", result: true },
    { input: "2 != 1", result: true },
    { input: "1 != 1", result: false },
    { input: "true != false", result: true },
    { input: "true != true", result: false },
  ],
};

describe(`Runtime: logical operators`, () => {
  Object.entries(testCases).forEach((entry) => {
    describe(entry[0], () => {
      const testCases = entry[1];
      testCases.forEach((test) => {
        it(`${test.input}`, () => {
          expect(executeExpression(test.input)).toEqual(test.result);
        });
      });
    });
  });
});
