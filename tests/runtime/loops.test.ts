import { beforeEach, describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";
import { ExecutionScope } from "../../runtime/scope";

const globalScope = new ExecutionScope();

const fibDefinition = `
const fib = n:int -> {
    var a = 0;
    var b = 1;
    var result = 1;
    if (n == 0) {
        result = 0;
    } else {
        for (var i = 2; i <= n; i = i + 1)  {
            result = a + b;
            a = b;
            b = result;
        }
    } 
    return result;
};
`;

const testCases = [
  {
    name: "fib(0)",
    input: `
        ${fibDefinition}
        const result = fib(0);
      `,
    result: 0,
  },
  {
    name: "fib(1)",
    input: `
        ${fibDefinition}
        const result = fib(1);
      `,
    result: 1,
  },
  {
    name: "fib(6)",
    input: `
        ${fibDefinition}
        const result = fib(6);
      `,
    result: 8,
  },
  {
    name: "fib(10)",
    input: `
        ${fibDefinition}
        const result = fib(10);
      `,
    result: 55,
  },
];

describe(`Runtime: loops`, () => {
  beforeEach(() => globalScope.clear());
  testCases.forEach((test) => {
    it(`${test.name}`, () => {
      executeProgram(test.input, globalScope);
      expect(globalScope.getVariable("result").value).toEqual(test.result);
    });
  });
});
