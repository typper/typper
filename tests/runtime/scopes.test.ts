import { beforeEach, describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";
import { deepDereference, ExecutionScope } from "../../runtime/scope";

describe(`Runtime: variable scopes`, () => {
  const globalScope = new ExecutionScope();
  beforeEach(() => {
    globalScope.clear();
  });
  // FIXME:
  // it("should not allow name shadowing", () => {
  //   expect(() => {
  //     executeProgram(`const a = 12; { const a = 3; }`, globalScope);
  //   }).toThrow();
  // });
  it("should not leak variables between scopes", () => {
    expect(() => {
      executeProgram(
        `const a = 12; { const b = 3; } const c = a + b;`,
        globalScope,
      );
    }).toThrow();
  });
  it("should allow same variable names between sibling blocks", () => {
    executeProgram(
      `var a = 12; { const b = 3; a = a + b; } { const b = 2; a = a + b;}`,
      globalScope,
    );
    expect(deepDereference(globalScope.getVariable("a"))).toEqual(17);
  });
});
