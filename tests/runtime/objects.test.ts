import { beforeEach, describe, expect, it } from "@jest/globals";
import { deepDereference, ExecutionScope } from "../../runtime/scope";
import { executeProgram } from "../../runtime/run";

type RuntimeTest = {
  name: string;
  input: string;
  result: any;
};

const globalScope = new ExecutionScope();
const testCases: RuntimeTest[] = [
  {
    name: "Anonymous object",
    input: "const result = { name: '' };",
    result: { name: "" },
  },
  {
    name: "Object literal type",
    input: "const result: { name: string } = { name: '' };",
    result: { name: "" },
  },
  {
    name: "Nested objects",
    input:
      "const result: { name: string, data: { age: int, size: int } } = { name: '', data: { age: 2, size: 12 } };",
    result: { name: "", data: { age: 2, size: 12 } },
  },
  {
    name: "Method property call",
    input: `
        const object: { getValue: -> int } = { getValue: -> 2 };
        const result = object.getValue();
      `,
    result: 2,
  },
  {
    name: "Dynamic object value",
    input: "const result = { name: 'a' + 'b' };",
    result: { name: "ab" },
  },
];

describe(`Runtime: objects`, () => {
  beforeEach(() => globalScope.clear());
  testCases.forEach((test) => {
    it(`${test.name}`, () => {
      executeProgram(test.input, globalScope);
      expect(deepDereference(globalScope.getVariable("result"))).toEqual(
        test.result,
      );
    });
  });
});
