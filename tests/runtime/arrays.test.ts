import { beforeEach, describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";
import { deepDereference, ExecutionScope } from "../../runtime/scope";

const globalScope = new ExecutionScope();
const testCases = [
  {
    name: "Basic array creation",
    input: `
        const result = [1, 2, 3 + 5];
      `,
    result: [1, 2, 8],
  },
  {
    name: "Multi-dimensional array",
    input: `
        const result = [[1,2,3], [4,5,6], [7,8,9]];
      `,
    result: [
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9],
    ],
  },
  {
    name: "Basic array access",
    input: `
        const array = [1,2,3,4,5];
        const result = array[4];
      `,
    result: 5,
  },
  {
    name: "Inline array access",
    input: `
        const result = [1,2,3,4,5][4];
      `,
    result: 5,
  },
  {
    name: "Nested indices",
    input: `
        const result = [2,4,6,8,10][[0,1,2,3,4][4]];
      `,
    result: 10,
  },
  {
    name: "Function result array access",
    input: `
        const array = a: int -> [a * 0, a * 1, a * 2, a * 3];
        const result = array(50)[2];
      `,
    result: 100,
  },
  {
    name: "Array element assignment",
    input: `
        const array = [1, 2, 3];
        array[0] = 3;
        const result = array;
      `,
    result: [3, 2, 3],
  },
  {
    name: "Array element assignment",
    input: `
        const a = 3;
        const array = [1, 2, a];
        array[2] = 15;
        const result = array[2] + a;
      `,
    result: 18,
  },
];

describe(`Runtime: arrays`, () => {
  beforeEach(() => globalScope.clear());
  testCases.forEach((test) => {
    it(`${test.name}`, () => {
      executeProgram(test.input, globalScope);
      expect(deepDereference(globalScope.getVariable("result"))).toEqual(
        test.result,
      );
    });
  });
});
