import { beforeEach, describe, expect, it } from "@jest/globals";
import { executeProgram } from "../../runtime/run";
import { ExecutionScope } from "../../runtime/scope";

const globalScope = new ExecutionScope();
const testCases = [
  {
    name: "Pass functions as arguments",
    input: `
        const compose = x:fn, y:fn, z:fn -> {
          x(y(z()));
        };
        const a:(int->int) = n:int -> n * n;
        const b:(int->int) = n:int -> 2 * n;
        const c:(->int) = -> 1;
        const result = compose(a, b, c);
      `,
    result: 4,
  },
  {
    name: "Function with typed function argument",
    input: `
        const compose = f:(int -> int) -> f(2);
        const f:(int->int) = n:int -> n * 12;
        const result = compose(f);
      `,
    result: 24,
  },
];

describe(`Runtime: function composition`, () => {
  beforeEach(() => globalScope.clear());
  testCases.forEach((test) => {
    it(`${test.name}`, () => {
      executeProgram(test.input, globalScope);
      expect(globalScope.getVariable("result").value).toEqual(test.result);
    });
  });
});
