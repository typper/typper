import { describe, expect, it } from "@jest/globals";
import { zip } from "../../ts-utils/zip";

describe(`Utils: array zip`, () => {
  const testCases = [
    {
      name: "First longer",
      first: [1, 1, 1],
      second: [2, 2],
      result: [1, 2, 1, 2, 1],
    },
    {
      name: "Second longer",
      first: [1, 1],
      second: [2, 2, 2],
      result: [1, 2, 1, 2, 2],
    },
    {
      name: "Equal lengths",
      first: [1, 1, 1],
      second: [2, 2, 2],
      result: [1, 2, 1, 2, 1, 2],
    },
    { name: "First empty", first: [], second: [2, 2, 2], result: [2, 2, 2] },
    { name: "Second empty", first: [1, 1, 1], second: [], result: [1, 1, 1] },
    { name: "Both empty", first: [], second: [], result: [] },
    {
      name: "Zip check",
      first: [1, 2, 3],
      second: [4, 5, 6],
      result: [1, 4, 2, 5, 3, 6],
    },
  ];
  testCases.forEach((test) => {
    const { first, second, name, result } = test;
    it(`${name}`, () => {
      expect(zip(first, second)).toEqual(result);
    });
  });
});
