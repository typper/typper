import { describe, expect, it } from "@jest/globals";
import { parseProgram } from "../../parser/parser";
import { checkTypes } from "../../typechecker/type-checker";
import { InvalidTypeError } from "../../typechecker/errors/invalid-type-error";

type TestCase = {
  name: string;
  input: string;
};

describe(`Typecheck: invalid basic variable types`, () => {
  const testCases: TestCase[] = [
    {
      name: "Strings",
      input: "const x: string = 0;",
    },
    {
      name: "Integers",
      input: "const x: int = 1.2;",
    },
    {
      name: "Long integer",
      input: "const x: long = true;",
    },
    {
      name: "Float",
      input: "const f: float = '';",
    },
    {
      name: "Double precision",
      input: "const f: float = false;",
    },
    {
      name: "Boolean",
      input: "const f: bool = 12;",
    },
    {
      name: "Function type for variable",
      input: "const f: int -> int = x:int -> 2.1 * x;",
    },
    {
      name: "Anonymous function with return type",
      input: "const f: (-> int) = -> 2.0;",
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      const ast = parseProgram(test.input);
      expect(() => checkTypes(ast)).toThrow(InvalidTypeError);
    });
  });
});
