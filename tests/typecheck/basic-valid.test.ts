import { describe, expect, it } from "@jest/globals";
import { parseProgram } from "../../parser/parser";
import { checkTypes } from "../../typechecker/type-checker";

type TestCase = {
  name: string;
  input: string;
};

describe(`Typecheck: valid basic variable types`, () => {
  const testCases: TestCase[] = [
    {
      name: "String",
      input: "const x: string = 'simple string';",
    },
    {
      name: "Integer",
      input: "const x: int = 12;",
    },
    {
      name: "Long integer",
      input: "const x: int = 3147483647;",
    },
    {
      name: "Float",
      input: "const f: double = 1.23;",
    },
    {
      name: "Double precision",
      input: "const f: double = 1.2312121212121212;",
    },
    {
      name: "Boolean",
      input: "const f: bool = false;",
    },
    {
      name: "Function type for variable",
      input: "const f: (int -> int) = x:int -> x * 2;",
    },
    {
      name: "Anonymous function with return type",
      input: "const f: (-> int) = -> 2;",
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      const ast = parseProgram(test.input);
      expect(() => checkTypes(ast)).not.toThrow();
      expect(ast).toMatchSnapshot();
    });
  });
});
