import { describe, expect, it } from "@jest/globals";
import { TokenPosition } from "typescript-parsec/lib/Lexer";
import { matchTypes } from "../../typechecker/type-checker";
import { createArrayType, Type } from "../../typechecker/types";
import { TypeInfo } from "../../typechecker/type-info";

type TestCase = {
  name: string;
  left: TypeInfo;
  right: TypeInfo;
  expected: boolean;
};

const fakePosition: TokenPosition = {
  columnBegin: 0,
  columnEnd: 0,
  index: 0,
  rowBegin: 0,
  rowEnd: 0,
};

const intType = TypeInfo.create("int", [], fakePosition);

describe(`Typecheck: valid basic variable types`, () => {
  const testCases: TestCase[] = [
    {
      name: "Two simple types",
      left: intType,
      right: intType,
      expected: true,
    },
    {
      name: "Two methods int -> int",
      left: TypeInfo.create("fn", [intType, intType], fakePosition),
      right: TypeInfo.create("fn", [intType, intType], fakePosition),
      expected: true,
    },
    {
      name: "Two arrays - equal types, equal length",
      left: createArrayType(Type.int, 5),
      right: createArrayType(Type.int, 5),
      expected: true,
    },
    {
      name: "Two arrays - equal types, different length",
      left: createArrayType(Type.int, 5),
      right: createArrayType(Type.int, 10),
      expected: false,
    },
    {
      name: "Two arrays - different types, equal length",
      left: createArrayType(Type.int, 5),
      right: createArrayType(Type.bool, 5),
      expected: false,
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.left.name} <- ${test.right.name}`, () => {
      const result = matchTypes(test.left, test.right);
      expect(result).toBe(test.expected);
    });
  });
});
