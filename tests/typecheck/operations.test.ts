import { describe, expect, it } from "@jest/globals";
import { parseProgram } from "../../parser/parser";
import { checkTypes } from "../../typechecker/type-checker";

type TestCase = {
  name: string;
  input: string;
};

describe(`Typecheck: valid basic variable types`, () => {
  const testCases: TestCase[] = [
    {
      name: "string + int",
      input: "const result = 'a' + 2;",
    },
    {
      name: "float + int",
      input: "const result = 2.2 + 2;",
    },
    {
      name: "bool + int",
      input: "const result = true + 2;",
    },
    {
      name: "bool + float",
      input: "const result = true + 2.2;",
    },
    {
      name: "bool + string",
      input: "const result = true + 'x';",
    },
    {
      name: "Comparison - bool vs int",
      input: "const result = true == 2;",
    },
    {
      name: "Comparison - bool vs string",
      input: "const result = true == 'x';",
    },
    {
      name: "Comparison - bool vs float",
      input: "const result = true == 2.2;",
    },
  ];
  testCases.forEach((test) => {
    it(`${test.name}: ${test.input}`, () => {
      const ast = parseProgram(test.input);
      expect(() => checkTypes(ast)).toThrow();
      expect(ast).toMatchSnapshot();
    });
  });
});
