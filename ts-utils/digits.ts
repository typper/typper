export function digits(number: number) {
  return number.toString().length;
}
