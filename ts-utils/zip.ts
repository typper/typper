export const zip = <T>(first: T[], second: T[]): T[] => {
  const length = Math.min(first.length, second.length);
  const result: T[] = [];
  for (let i = 0; i < length; i++) {
    result.push(first[i]);
    result.push(second[i]);
  }
  return result.concat(first.slice(length), second.slice(length));
};
