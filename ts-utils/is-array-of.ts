export const isArray = <T, X>(x: T[] | X): x is Array<T> => {
  return (x as any)?.length >= 0;
};
