export function dumpCode(params: any[]) {
  return params
    .flat()
    .map((part) => {
      return `${part?.code ? part.code : part?.text}`;
    })
    .join(" ");
}
