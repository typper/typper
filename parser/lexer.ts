import {
  alt,
  buildLexer,
  Lexer,
  Parser,
  tok,
  Token as ParsecToken,
} from "typescript-parsec";

export enum TokenKind {
  Integer = "Integer",
  Decimal = "Decimal",
  True = "True",
  False = "False",
  Add = "Add",
  Sub = "Sub",
  Mul = "Mul",
  Div = "Div",
  LSquareBracket = "LSquareBracket",
  RSquareBracket = "RSquareBracket",
  LParen = "LParen",
  RParen = "RParen",
  LBrace = "LBrace",
  RBrace = "RBrace",
  Space = "Space",
  Const = "Const",
  Var = "Var",
  Identifier = "Identifier",
  Colon = "Colon",
  Semicolon = "Semicolon",
  Assign = "Assign",
  Arrow = "Arrow",
  Comma = "Comma",
  Dot = "Dot",
  Exclamation = "Exclamation",
  Tab = "Tab",

  Equal = "Equal",
  Less = "Less",
  Greater = "Greater",
  GreaterEqual = "GreaterEqual",
  LessEqual = "LessEqual",
  NotEqual = "NotEqual",
  And = "And",
  Or = "Or",

  If = "If",
  Else = "Else",
  For = "For",
  Return = "Return",

  String = "String",
  StringTemplate = "StringTemplate",
}

export type TokenTypesMap<T, TokenKindType extends string | number | symbol> = {
  [key in TokenKindType]: T;
};

export const TokenRules: TokenTypesMap<[boolean, RegExp], TokenKind> = {
  [TokenKind.True]: [true, /^true/g],
  [TokenKind.False]: [true, /^false/g],

  [TokenKind.Decimal]: [true, /^\d+\.\d+/g],
  [TokenKind.Integer]: [true, /^\d+/g],

  [TokenKind.Add]: [true, /^\+/g],
  [TokenKind.Sub]: [true, /^-/g],
  [TokenKind.Mul]: [true, /^\*/g],
  [TokenKind.Div]: [true, /^\//g],
  [TokenKind.LParen]: [true, /^\(/g],
  [TokenKind.RParen]: [true, /^\)/g],
  [TokenKind.LSquareBracket]: [true, /^\[/g],
  [TokenKind.RSquareBracket]: [true, /^]/g],
  [TokenKind.LBrace]: [true, /^\{/g],
  [TokenKind.RBrace]: [true, /^}/g],
  [TokenKind.Const]: [true, /^const/g],
  [TokenKind.Var]: [true, /^var/g],
  [TokenKind.Colon]: [true, /^:/g],
  [TokenKind.Semicolon]: [true, /^;/g],
  [TokenKind.Assign]: [true, /^=/g],
  [TokenKind.Arrow]: [true, /^->/g],
  [TokenKind.Comma]: [true, /^,/g],
  [TokenKind.Dot]: [true, /^\./g],

  [TokenKind.Space]: [false, /^\s+/g],
  [TokenKind.Tab]: [false, /^\t+/g],

  [TokenKind.Equal]: [true, /^==/g],
  [TokenKind.Less]: [true, /^</g],
  [TokenKind.Greater]: [true, /^>/g],
  [TokenKind.GreaterEqual]: [true, /^>=/g],
  [TokenKind.LessEqual]: [true, /^<=/g],
  [TokenKind.NotEqual]: [true, /^!=/g],

  [TokenKind.Exclamation]: [true, /^!/g],

  [TokenKind.And]: [true, /^&&/g],
  [TokenKind.Or]: [true, /^\|\|/g],

  [TokenKind.If]: [true, /^if/g],
  [TokenKind.Else]: [true, /^else/g],
  [TokenKind.For]: [true, /^for/g],
  [TokenKind.Return]: [true, /^return/g],

  [TokenKind.String]: [true, /^'[^'\\]*(?:\\.[^'\\]*)*'/g],
  [TokenKind.StringTemplate]: [true, /^`[^`\\]*(?:\\.[^`\\]*)*`/g],
  [TokenKind.Identifier]: [true, /^[$a-zA-Z][a-zA-Z0-9_]*/g],
};

export const lexer: Lexer<TokenKind> = buildLexer(
  Object.entries(TokenRules).map((entry) => [
    ...entry[1],
    entry[0] as TokenKind,
  ]),
);

export const Token = Object.fromEntries(
  Object.keys(TokenRules).map((val) => [val, tok(val)]),
) as TokenTypesMap<Parser<TokenKind, ParsecToken<TokenKind>>, TokenKind>;

export const LogicalOperatorKinds = [
  TokenKind.And,
  TokenKind.Equal,
  TokenKind.Greater,
  TokenKind.GreaterEqual,
  TokenKind.Less,
  TokenKind.LessEqual,
  TokenKind.NotEqual,
  TokenKind.Or,
] as const;

export const LogicalOperators = alt(
  Token.And,
  Token.Equal,
  Token.Greater,
  Token.GreaterEqual,
  Token.Less,
  Token.LessEqual,
  Token.NotEqual,
  Token.Or,
);

export const UnaryOperators = [
  TokenKind.Add,
  TokenKind.Sub,
  TokenKind.Exclamation,
];

export type UnaryOperatorKind =
  | TokenKind.Add
  | TokenKind.Sub
  | TokenKind.Exclamation;

function dumpTokens(firstToken: ParsecToken<TokenKind>) {
  let result: TokenKind[] = [];
  let token = firstToken;
  do {
    result.push(token.kind);
  } while ((token = token.next));
  return result;
}
