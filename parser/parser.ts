import {
  alt,
  alt_sc,
  apply,
  expectEOF,
  expectSingleResult,
  kleft,
  kmid,
  kright,
  list_sc,
  opt,
  opt_sc,
  Parser,
  rep_sc,
  Rule,
  rule,
  seq,
  tok,
} from "typescript-parsec";
import {
  ArrayAccessNode,
  ArrayNode,
  BaseNode,
  BinaryNode,
  BlockNode,
  BooleanNode,
  DeclarationNode,
  ExpressionNode,
  ForLoopNode,
  FunctionArgumentNode,
  FunctionCallNode,
  FunctionNode,
  IdentifierNode,
  IfBranchNode,
  NegationNode,
  NumberNode,
  ObjectAccessNode,
  ObjectNode,
  ObjectPropertyNode,
  ProgramNode,
  ReturnNode,
  StringArgumentNode,
  StringNode,
  StringTemplateNode,
} from "./ast";
import { lexer, Token, TokenKind, UnaryOperatorKind } from "./lexer";
import { notEmpty } from "../ts-utils/not-empty";
import { dumpAST } from "./debug/ast";
import { dumpCode } from "./dump-code";
import { stringTemplateLexer } from "./string-template/lexer";
import { STRING_TEMPLATE_CONTENT } from "./string-template/parser";
import { TokenPosition } from "typescript-parsec/lib/Lexer";
import { ObjectTypeInfo, TypeInfo } from "../typechecker/type-info";
import { isArray } from "../ts-utils/is-array-of";
import { dumpLexed } from "./debug/lexer";

const EXPRESSION = rule<TokenKind, ExpressionNode>();
const EXPRESSION_STATEMENT = rule<TokenKind, ExpressionNode>();
const EXPRESSION_CONDITIONAL = rule<TokenKind, ExpressionNode>();
const UNARY_EXPRESSION = rule<TokenKind, ExpressionNode>();
const FACTOR = rule<TokenKind, ExpressionNode>();
const FUNCTION = rule<TokenKind, FunctionNode>();
const FUNCTION_ARGUMENT = rule<TokenKind, FunctionArgumentNode>();
const FUNCTION_ARGUMENTS = rule<TokenKind, FunctionArgumentNode[]>();
const FUNCTION_CALL = rule<
  TokenKind,
  FunctionCallNode | ObjectAccessNode | ArrayAccessNode
>();
const FUNCTION_CALL_PART = rule<
  TokenKind,
  FunctionCallNode | ObjectAccessNode | ArrayAccessNode
>();
const ARGUMENT_LIST = rule<TokenKind, ExpressionNode[]>();

const UNARY_OPERATOR = rule<TokenKind, UnaryOperatorKind>();
const LOGICAL_OR_EXPRESSION = rule<TokenKind, ExpressionNode>();
const LOGICAL_AND_EXPRESSION = rule<TokenKind, ExpressionNode>();
const EQUALITY_EXPRESSION = rule<TokenKind, ExpressionNode>();
const RELATIONAL_EXPRESSION = rule<TokenKind, ExpressionNode>();

const EXP = rule<TokenKind, ExpressionNode>();
const DECLARATION = rule<TokenKind, DeclarationNode>();
const ASSIGNMENT_EXPRESSION = rule<TokenKind, ExpressionNode>();
const INITIALIZER = rule<TokenKind, ExpressionNode>();
const BLOCK = rule<TokenKind, BlockNode>();
const IF_BLOCK = rule<TokenKind, IfBranchNode>();
const ELSEIF_BLOCK = rule<TokenKind, IfBranchNode>();
const ELSE_BLOCK = rule<TokenKind, IfBranchNode>();
const FOR_BLOCK = rule<TokenKind, ForLoopNode>();
const STATEMENT = rule<TokenKind, BaseNode>();
const PROGRAM_BLOCK = rule<TokenKind, ProgramNode>();
const STRING = rule<TokenKind, StringNode>();
const STRING_TEMPLATE = rule<TokenKind, StringTemplateNode>();
const ARRAY_LITERAL = rule<TokenKind, ArrayNode>();
const ARRAY_ACCESS = rule<TokenKind, ArrayAccessNode>();

const OBJECT_LITERAL = rule<TokenKind, ObjectNode>();
const OBJECT_PROPERTY = rule<TokenKind, ObjectPropertyNode>();
const OBJECT_ACCESS = rule<TokenKind, ObjectAccessNode>();

const RETURN = rule<TokenKind, ReturnNode>();

const TYPE = rule<TokenKind, TypeInfo>();
const TYPE_IDENTIFIER = rule<TokenKind, TypeInfo>();
const TYPE_EXPRESSION = rule<TokenKind, TypeInfo>();
const TYPE_OBJECT_EXPRESSION = rule<TokenKind, TypeInfo>();
const TYPE_OBJECT_PROPERTY = rule<TokenKind, [string, TypeInfo]>();
const TYPE_FUNC_EXPRESSION = rule<TokenKind, TypeInfo>();
const TYPE_FUNC_EXPRESSION_CHILD = rule<TokenKind, TypeInfo>();
const FUNCTION_RETURN_TYPE = rule<TokenKind, TypeInfo>();

const LHS_EXPRESSION = rule<TokenKind, ExpressionNode>();
const MEMBER_EXPRESSION = rule<TokenKind, ExpressionNode>();
const MEMBER_EXPRESSION_PART = rule<
  TokenKind,
  ArrayAccessNode | ObjectAccessNode
>();
const PRIMARY_EXPRESSION = rule<TokenKind, ExpressionNode>();
const PARENTHESIS_EXPRESSION = rule<TokenKind, ExpressionNode>();

const IDENTIFIER = rule<TokenKind, IdentifierNode>();
const LITERAL = rule<TokenKind, ExpressionNode>();
const BOOLEAN_LITERAL = rule<TokenKind, ExpressionNode>();
const NUMERIC_LITERAL = rule<TokenKind, ExpressionNode>();

LHS_EXPRESSION.setPattern(alt_sc(FUNCTION_CALL, MEMBER_EXPRESSION));

MEMBER_EXPRESSION_PART.setPattern(alt_sc(OBJECT_ACCESS, ARRAY_ACCESS));

MEMBER_EXPRESSION.setPattern(
  apply(
    seq(
      alt_sc(FUNCTION, PRIMARY_EXPRESSION),
      opt_sc(rep_sc(MEMBER_EXPRESSION_PART)),
    ),
    ([arrayOrObject, memberAccess]) => {
      if (memberAccess.length === 0) {
        return arrayOrObject;
      }
      let firstPart = arrayOrObject;
      memberAccess.forEach((access) => {
        access.baseExpression = firstPart;
        firstPart = access;
      });
      return firstPart;
    },
  ),
);

PRIMARY_EXPRESSION.setPattern(
  alt_sc(
    OBJECT_LITERAL,
    IDENTIFIER,
    ARRAY_LITERAL,
    PARENTHESIS_EXPRESSION,
    BOOLEAN_LITERAL,
    NUMERIC_LITERAL,
    STRING,
    STRING_TEMPLATE,
  ),
);

PARENTHESIS_EXPRESSION.setPattern(
  apply(seq(Token.LParen, EXPRESSION, Token.RParen), (params) => {
    const [lparen, expression] = params;
    return {
      name: "parenthesis",
      expression,
      pos: lparen.pos,
      code: dumpCode(params),
    };
  }),
);

IDENTIFIER.setPattern(
  apply(Token.Identifier, (identifier) => {
    return {
      name: "identifier",
      variableName: identifier.text,
      pos: identifier.pos,
      code: dumpCode([identifier]),
    };
  }),
);

ARRAY_LITERAL.setPattern(
  apply(
    seq(
      Token.LSquareBracket,
      list_sc(EXPRESSION, Token.Comma),
      Token.RSquareBracket,
    ),
    (params, tokenRange) => {
      const [, elements] = params;
      const codeLocation = getCodeLocation(tokenRange);
      return {
        name: "array",
        typeInfo: TypeInfo.create(
          "array",
          [
            TypeInfo.create("infer", [], codeLocation),
            TypeInfo.create(
              "length",
              [TypeInfo.create(`${elements.length}`, [], codeLocation)],
              codeLocation,
            ),
          ],
          codeLocation,
        ),
        elements,
        code: dumpCode(params),
        pos: params[0].pos,
      };
    },
  ),
);

ARRAY_ACCESS.setPattern(
  apply(
    kmid(Token.LSquareBracket, EXPRESSION, Token.RSquareBracket),
    (indexExpression, tokenRange) => {
      return {
        name: "array_access",
        baseExpression: null,
        indexExpression,
        code: dumpCode([indexExpression]),
        pos: getCodeLocation(tokenRange),
      };
    },
  ),
);

OBJECT_LITERAL.setPattern(
  apply(
    seq(Token.LBrace, list_sc(OBJECT_PROPERTY, Token.Comma), Token.RBrace),
    ([, properties], tokenRange) => {
      return {
        name: "object",
        properties,
        code: dumpCode(tokenRange),
        pos: getCodeLocation(tokenRange),
      };
    },
  ),
);

OBJECT_ACCESS.setPattern(
  apply(seq(Token.Dot, Token.Identifier), ([, property], tokenRange) => {
    return {
      name: "object_access",
      baseExpression: null,
      property: property.text,
      code: dumpCode(tokenRange),
      pos: getCodeLocation(tokenRange),
    };
  }),
);

OBJECT_PROPERTY.setPattern(
  apply(
    seq(Token.Identifier, Token.Colon, EXPRESSION),
    ([name, , initialValue], tokenRange) => {
      return {
        name: "object_property",
        propertyName: name.text,
        initialValue,
        code: dumpCode(tokenRange),
        pos: getCodeLocation(tokenRange),
      };
    },
  ),
);

TYPE_IDENTIFIER.setPattern(
  apply(Token.Identifier, (identifier) => {
    return TypeInfo.create(identifier.text, [], identifier.pos);
  }),
);

TYPE.setPattern(kright(Token.Colon, TYPE_EXPRESSION));

TYPE_EXPRESSION.setPattern(
  apply(
    alt(
      kmid(Token.LParen, TYPE_EXPRESSION, Token.RParen),
      TYPE_FUNC_EXPRESSION,
      TYPE_IDENTIFIER,
      TYPE_OBJECT_EXPRESSION,
    ),
    (typeInfo) => typeInfo,
  ),
);

TYPE_OBJECT_PROPERTY.setPattern(
  apply(
    seq(Token.Identifier, Token.Colon, TYPE_EXPRESSION),
    ([propertyName, , propertyType]) => {
      return [propertyName.text, propertyType];
    },
  ),
);

TYPE_OBJECT_EXPRESSION.setPattern(
  apply(
    seq(Token.LBrace, list_sc(TYPE_OBJECT_PROPERTY, Token.Comma), Token.RBrace),
    ([, properties], tokenRange) => {
      return new ObjectTypeInfo(
        Object.fromEntries(properties),
        [],
        getCodeLocation(tokenRange),
      );
    },
  ),
);

TYPE_FUNC_EXPRESSION.setPattern(
  apply(
    seq(
      opt_sc(list_sc(TYPE_FUNC_EXPRESSION_CHILD, Token.Comma)),
      Token.Arrow,
      TYPE_EXPRESSION,
    ),
    ([argumentTypes, , returnType], tokenRange) => {
      return TypeInfo.create(
        "fn",
        [...(argumentTypes || []), returnType],
        getCodeLocation(tokenRange),
      );
    },
  ),
);

TYPE_FUNC_EXPRESSION_CHILD.setPattern(
  apply(
    alt_sc(kmid(Token.LParen, TYPE_EXPRESSION, Token.RParen), TYPE_IDENTIFIER),
    (typeInfo) => typeInfo,
  ),
);
INITIALIZER.setPattern(
  kmid(Token.Assign, ASSIGNMENT_EXPRESSION, Token.Semicolon),
);

DECLARATION.setPattern(
  apply(
    seq(
      alt_sc(Token.Const, Token.Var),
      seq(Token.Identifier, opt_sc(TYPE), INITIALIZER),
    ),
    (params) => {
      const [modifier, sq] = params;
      const [identifier, typeInfo, assignment] = sq;

      return {
        name: "declaration",
        code: dumpCode(params),
        pos: modifier.pos,
        variableName: identifier.text,
        variableType: typeInfo,
        expression: assignment,
        modifier: modifier.text === "const" ? "const" : "var",
      };
    },
  ),
);

// TODO: https://tomcopeland.blogs.com/EcmaScript.html#prod23
ASSIGNMENT_EXPRESSION.setPattern(
  apply(
    /* LHSExpression */ alt_sc(
      seq(kleft(LHS_EXPRESSION, Token.Assign), ASSIGNMENT_EXPRESSION),
      EXPRESSION_CONDITIONAL,
    ),
    //TODO: TERNARY_EXPRESSION,
    (params, tokenRange) => {
      if (!isArray(params)) {
        return params;
      }
      const [leftHandSideExpression, expression] = params;
      return {
        name: "assignment",
        pos: getCodeLocation(tokenRange),
        expression: expression,
        code: dumpCode(params),
        leftHandSideExpression,
      };
    },
  ),
);

STRING.setPattern(
  apply(Token.String, (value, tokenRange) => {
    return {
      name: "string",
      typeInfo: TypeInfo.create("string", [], getCodeLocation(tokenRange)),
      value: value.text.slice(1, value.text.length - 1),
      code: value.text,
      pos: getCodeLocation(tokenRange),
    };
  }),
);

STRING_TEMPLATE.setPattern(
  apply(Token.StringTemplate, (value, tokenRange) => {
    const tokens = stringTemplateLexer.parse(value.text.trim());
    const stringTemplate = expectSingleResult(
      STRING_TEMPLATE_CONTENT.parse(tokens),
    );

    return {
      name: "string_template",
      typeInfo: TypeInfo.create("string", [], getCodeLocation(tokenRange)),
      value: stringTemplate.value,
      arguments: stringTemplate.arguments.map(
        (argument: StringArgumentNode) => ({
          ...argument,
          expression: expectSingleResult(
            ASSIGNMENT_EXPRESSION.parse(
              lexer.parse((argument.expression as StringNode).value),
            ),
          ),
        }),
      ),
      parts: stringTemplate.parts,
      code: dumpCode([value]),
      pos: getCodeLocation(tokenRange),
    };
  }),
);

NUMERIC_LITERAL.setPattern(
  apply(
    alt_sc(Token.Integer, Token.Decimal),
    (value, tokenRange): NumberNode => {
      return {
        name: value.kind === TokenKind.Integer ? "integer" : "decimal",
        typeInfo: TypeInfo.create(
          value.kind === TokenKind.Integer ? "int" : "double",
          [],
          getCodeLocation(tokenRange),
        ),
        value: value.text,
        code: value.text,
        pos: getCodeLocation(tokenRange),
      };
    },
  ),
);

BOOLEAN_LITERAL.setPattern(
  apply(alt(Token.True, Token.False), (value, tokenRange): BooleanNode => {
    return {
      name: "boolean",
      typeInfo: TypeInfo.create("bool", [], getCodeLocation(tokenRange)),
      value: value.text === "true",
      code: value.text,
      pos: getCodeLocation(tokenRange),
    };
  }),
);

LITERAL.setPattern(
  alt_sc(STRING, STRING_TEMPLATE, BOOLEAN_LITERAL, NUMERIC_LITERAL),
);

UNARY_OPERATOR.setPattern(
  apply(
    alt_sc(Token.Add, Token.Sub, Token.Exclamation),
    (token) => token.kind as UnaryOperatorKind,
  ),
);

/*
UNARY_EXPRESSION
  = ('+' | '-') UNARY_EXPRESSION
*/
UNARY_EXPRESSION.setPattern(
  alt_sc(
    apply(
      seq(UNARY_OPERATOR, UNARY_EXPRESSION),
      (params, tokenRange): ExpressionNode | NegationNode => {
        const [operator, value] = params;
        switch (operator) {
          case TokenKind.Add:
            return value;
          case TokenKind.Sub:
            return {
              name: "negation",
              expression: value,
              code: dumpCode(params),
              pos: tokenRange[0]?.pos,
            };
          default:
            throw new Error(`Unknown unary operator: ${value[0].text}`);
        }
      },
    ),
    LHS_EXPRESSION,
  ),
);

/*
FACTOR
  = UNARY_EXPRESSION
  = FACTOR ('*' | '/') UNARY_EXPRESSION
*/
FACTOR.setPattern(
  apply(
    seq(
      UNARY_EXPRESSION,
      rep_sc(seq(alt_sc(Token.Mul, Token.Div), UNARY_EXPRESSION)),
    ),
    (params): ExpressionNode => {
      if (params[1].length === 0) {
        return params[0];
      }
      const [left, parts] = params;
      const types = {
        "*": "multiplication",
        "/": "division",
      } as const;
      let result: ExpressionNode = left;
      do {
        const [operator, factor] = parts.pop();
        result = {
          name: types[operator.text],
          left: result,
          right: factor,
          pos: operator.pos,
          code: dumpCode([operator, factor]),
        } as BinaryNode;
      } while (parts.length > 0);
      return result;
    },
  ),
);

/*
EXP
  = FACTOR
  = EXP ('+' | '-') FACTOR
*/
EXP.setPattern(
  apply(
    seq(FACTOR, rep_sc(seq(alt_sc(Token.Add, Token.Sub), FACTOR))),
    (params, tokenRange): ExpressionNode => {
      if (params[1].length === 0) {
        return params[0];
      }
      const [left, parts] = params;
      const types = {
        "+": "addition",
        "-": "subtraction",
      } as const;
      let result: ExpressionNode = left;
      do {
        const [operator, factor] = parts.pop();
        result = {
          name: types[operator.text],
          left: result,
          right: factor,
          pos: operator.pos,
          code: dumpCode([left, ...parts]),
        } as BinaryNode;
      } while (parts.length > 0);
      return result;
    },
  ),
);

function binaryExpressionPart<R extends ExpressionNode>(
  SUB_EXPRESSION: Rule<TokenKind, any>, // FIXME: any
  tokens: TokenKind[],
): Parser<TokenKind, R> {
  return apply(
    seq(
      SUB_EXPRESSION,
      opt_sc(seq(alt_sc.apply(null, tokens.map(tok)), SUB_EXPRESSION)),
    ),
    (params: any, tokenRange) => {
      const [left, rest] = params;
      if (!rest) {
        return left;
      }
      const [operator, right] = rest;
      return {
        name: "comparison",
        typeInfo: TypeInfo.create("bool", [], getCodeLocation(tokenRange)),
        code: dumpCode(params),
        pos: getCodeLocation(tokenRange),
        left,
        right,
        operator: operator.kind,
      };
    },
  );
}

RELATIONAL_EXPRESSION.setPattern(
  binaryExpressionPart(EXP, [
    TokenKind.Greater,
    TokenKind.GreaterEqual,
    TokenKind.Less,
    TokenKind.LessEqual,
  ]),
);

EQUALITY_EXPRESSION.setPattern(
  binaryExpressionPart(RELATIONAL_EXPRESSION, [
    TokenKind.Equal,
    TokenKind.NotEqual,
  ]),
);

LOGICAL_AND_EXPRESSION.setPattern(
  binaryExpressionPart(EQUALITY_EXPRESSION, [TokenKind.And]),
);

LOGICAL_OR_EXPRESSION.setPattern(
  binaryExpressionPart(LOGICAL_AND_EXPRESSION, [TokenKind.Or]),
);

EXPRESSION_CONDITIONAL.setPattern(LOGICAL_OR_EXPRESSION);

EXPRESSION.setPattern(ASSIGNMENT_EXPRESSION);

ELSEIF_BLOCK.setPattern(
  apply(
    seq(Token.Else, Token.If, Token.LParen, EXPRESSION, Token.RParen, BLOCK),
    (params) => {
      const [, , , condition, , body] = params;
      return {
        name: "if_branch",
        condition,
        body,
        next: null,
        code: dumpCode(params),
        pos: params[0].pos,
      };
    },
  ),
);

ELSE_BLOCK.setPattern(
  apply(seq(Token.Else, BLOCK), (params) => {
    const [, body] = params;
    return {
      name: "if_branch",
      body,
      next: null,
      code: dumpCode(params),
      pos: params[0].pos,
    };
  }),
);

IF_BLOCK.setPattern(
  apply(
    seq(
      Token.If,
      Token.LParen,
      EXPRESSION,
      Token.RParen,
      BLOCK,
      rep_sc(ELSEIF_BLOCK),
      opt_sc(ELSE_BLOCK),
    ),
    (params) => {
      const [, , condition, , body, elseIfBranches, elseBranch] = params;
      const firstBranch: IfBranchNode = {
        name: "if_branch",
        condition,
        body,
        next: null,
        pos: params[0].pos,
        code: dumpCode(params),
      };
      const allBranches = [firstBranch, ...elseIfBranches, elseBranch].filter(
        notEmpty,
      );
      for (let i = 0; i < allBranches.length; i++) {
        allBranches[i].next = allBranches[i + 1];
      }
      return allBranches[0];
    },
  ),
);

RETURN.setPattern(
  apply(seq(Token.Return, EXPRESSION, Token.Semicolon), (params) => {
    const [returnToken, expression] = params;
    return {
      name: "return",
      code: dumpCode(params),
      pos: returnToken.pos,
      expression,
    };
  }),
);

FOR_BLOCK.setPattern(
  apply(
    seq(
      Token.For,
      Token.LParen,
      opt_sc(DECLARATION),
      opt_sc(EXPRESSION),
      Token.Semicolon,
      opt_sc(alt_sc(ASSIGNMENT_EXPRESSION, EXPRESSION)),
      Token.RParen,
      BLOCK,
    ),
    (params) => {
      const [, , declaration, condition, , iterate, , body] = params;
      return {
        name: "for_loop",
        declaration,
        condition,
        iterate,
        body,
        pos: params[0].pos,
        code: dumpCode(params),
      };
    },
  ),
);

BLOCK.setPattern(
  kmid(
    Token.LBrace,
    apply(rep_sc(STATEMENT), (params) => {
      return {
        name: "block",
        code: dumpCode(params),
        children: params,
        pos: params[0]?.pos,
      };
    }),
    Token.RBrace,
  ),
);

FUNCTION_ARGUMENT.setPattern(
  apply(seq(IDENTIFIER, opt_sc(TYPE)), ([identifier, type]) => {
    return {
      name: "function_argument",
      argumentName: identifier.variableName,
      type,
      code: dumpCode([identifier]),
      pos: identifier.pos,
    };
  }),
);

FUNCTION_ARGUMENTS.setPattern(
  apply(list_sc(FUNCTION_ARGUMENT, Token.Comma), (params) => {
    return params;
  }),
);

FUNCTION_RETURN_TYPE.setPattern(
  apply(seq(TYPE_EXPRESSION, Token.Colon), ([type]) => {
    return type;
  }),
);

FUNCTION.setPattern(
  apply(
    seq(opt(FUNCTION_ARGUMENTS), Token.Arrow, alt(EXPRESSION, BLOCK)),
    (params) => {
      const [functionArguments, arrow, body] = params;
      let returnBlock: BlockNode;
      if (body.name !== "block") {
        const returnExpression: ReturnNode = {
          code: dumpCode(params),
          pos: body.pos,
          name: "return",
          expression: body,
        };
        returnBlock = {
          children: [returnExpression],
          code: dumpCode(params),
          name: "block",
          pos: body.pos,
        };
      }
      return {
        name: "function",
        arguments: functionArguments ?? [],
        returnType: null,
        body: returnBlock || body,
        code: dumpCode(params),
        pos: functionArguments?.at(0)?.pos || arrow.pos,
      };
    },
  ),
);

ARGUMENT_LIST.setPattern(
  kmid(
    Token.LParen,
    opt_sc(list_sc(ASSIGNMENT_EXPRESSION, Token.Comma)),
    Token.RParen,
  ),
);

FUNCTION_CALL_PART.setPattern(
  apply(alt_sc(ARGUMENT_LIST, MEMBER_EXPRESSION_PART), (params, tokenRange) => {
    if (isArray(params)) {
      return {
        name: "function_call",
        arguments: params ?? [],
        baseExpression: null,
        code: dumpCode(params),
        pos: getCodeLocation(tokenRange),
      };
    }
    return params;
  }),
);

FUNCTION_CALL.setPattern(
  apply(
    seq(MEMBER_EXPRESSION, ARGUMENT_LIST, opt_sc(FUNCTION_CALL_PART)),
    (params, tokenRange) => {
      const [member, callArguments, rest] = params;

      const functionCall: FunctionCallNode = {
        name: "function_call",
        arguments: callArguments ?? [],
        baseExpression: member,
        code: dumpCode(params),
        pos: getCodeLocation(tokenRange),
      };

      if (rest) {
        rest.baseExpression = functionCall;
        return rest;
      }
      return functionCall;
    },
  ),
);

STATEMENT.setPattern(
  alt_sc(BLOCK, DECLARATION, EXPRESSION_STATEMENT, IF_BLOCK, FOR_BLOCK, RETURN),
);

EXPRESSION_STATEMENT.setPattern(kleft(EXPRESSION, Token.Semicolon));

PROGRAM_BLOCK.setPattern(
  apply(rep_sc(STATEMENT), (result) => ({
    name: "program",
    code: dumpCode(result),
    children: result,
    pos: result[0]?.pos,
  })),
);

function getCodeLocation<T extends { pos: TokenPosition }>([first, last]: [
  T,
  T,
]) {
  return {
    rowBegin: first.pos.rowBegin,
    columnBegin: first.pos.columnBegin,
    rowEnd: last?.pos.rowEnd,
    columnEnd: last?.pos.columnEnd,
    index: first.pos.index,
  };
}

export function parseProgram(program: string): any {
  let lexed = lexer.parse(program);
  const result = expectEOF(PROGRAM_BLOCK.parse(lexed));

  if (result.successful === true && result.candidates.length > 1) {
    console.log(dumpLexed(lexed));
    result.candidates.forEach((c) => {
      console.error(dumpAST(c.result));
    });
  }
  return expectSingleResult(result);
}

export function parseExpression(expression: string) {
  const lexed = lexer.parse(expression);
  return expectSingleResult(expectEOF(ASSIGNMENT_EXPRESSION.parse(lexed)));
}
