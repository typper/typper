import { TokenPosition } from "typescript-parsec/lib/Lexer";
import { Chalk } from "chalk";
import { digits } from "../../ts-utils/digits";

export function colorCode(
  code: string,
  { rowBegin, rowEnd, columnBegin, columnEnd }: TokenPosition,
) {
  const chalk = new Chalk();
  chalk.level = 3;
  let lines = code.split(`\n`);
  lines = lines.map((line, rowNumber) => {
    rowNumber += 1;
    if (rowNumber < rowBegin || rowNumber > rowEnd) {
      return line;
    }
    if (rowNumber > rowBegin && rowNumber < rowEnd) {
      return chalk.red(line);
    }
    if (rowBegin === rowEnd && rowBegin === rowNumber) {
      return (
        line.slice(0, columnBegin - 1) +
        chalk.red(line.slice(columnBegin - 1, columnEnd)) +
        line.slice(columnEnd)
      );
    }
    if (rowNumber === rowBegin) {
      return line.slice(0, columnBegin) + chalk.red(line.slice(columnBegin));
    }
    if (rowNumber === rowEnd) {
      return chalk.red(line.slice(0, columnBegin)) + line.slice(columnBegin);
    }
  });
  lines = lines.map((line, rowNumber) => {
    rowNumber += 1;
    let formattedNumber = `${rowNumber}`;
    let offset = digits(lines.length) + 2;
    if (rowNumber >= rowBegin && rowNumber <= rowEnd) {
      formattedNumber = `${chalk.red(">")}${formattedNumber.padStart(
        offset - 1,
      )}`;
    }
    formattedNumber = formattedNumber.padStart(offset, ` `);
    return chalk.gray(`${formattedNumber} | `) + line;
  });
  return lines.join("\n");
}
