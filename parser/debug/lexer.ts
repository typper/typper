import { Token as ParsecToken } from "typescript-parsec/lib/Lexer";

export function dumpLexed<T>(lexed: ParsecToken<T>): T[] {
  let token = lexed;
  const result = [];
  while (token) {
    result.push(token.kind);
    token = token.next;
  }
  return result;
}
