import {
  AdditionNode,
  ArrayAccessNode,
  ArrayNode,
  AssignmentNode,
  BaseNode,
  BlockNode,
  BooleanNode,
  ComparisonNode,
  DecimalNode,
  DeclarationNode,
  DivisionNode,
  ForLoopNode,
  FunctionArgumentNode,
  FunctionCallNode,
  FunctionNode,
  IdentifierNode,
  IfBranchNode,
  IntegerNode,
  MultiplicationNode,
  NativeCodeNode,
  NegationNode,
  NodeName,
  ObjectNode,
  ObjectPropertyNode,
  ParenthesisNode,
  ProgramNode,
  ReferenceNode,
  ReturnNode,
  StringArgumentNode,
  StringNode,
  StringTemplateNode,
  SubtractionNode,
  Visitor,
} from "../ast";
import { ExecutionScope } from "../../runtime/scope";
import { notEmpty } from "../../ts-utils/not-empty";

function dumpNode(
  node: BaseNode,
  content: any,
  properties: Record<string, string> = {},
) {
  return `<${node.name}${Object.entries(properties).map(
    ([k, v]) => ` ${k}="${v}"`,
  )}>${content ? `\n${content}\n` : ""}</${node.name}>`;
}

export class DebugVisitor implements Visitor<NodeName, ExecutionScope> {
  object_access: (node: BaseNode, scope: ExecutionScope) => any;
  private executionLog = [];

  identifier = (node: IdentifierNode, scope: ExecutionScope) =>
    dumpNode(node, [], { name: node.variableName });

  visit<T extends BaseNode>(node: T, scope: ExecutionScope): string | null {
    if (!node) {
      return null;
    }
    if (!this[node.name]) {
      throw new Error(`Missing node handler for type ${node.name}`);
    }
    const result = this[node.name](node as any, scope);
    this.executionLog.push({ node, scope, result });
    return result;
  }

  object(node: ObjectNode, scope: ExecutionScope) {
    return dumpNode(
      node,
      node.properties.map((property) => this.visit(property, scope)),
    );
  }

  object_property(node: ObjectPropertyNode, scope: ExecutionScope) {
    return dumpNode(node, [
      node.propertyName,
      this.visit(node.initialValue, scope),
    ]);
  }

  boolean(node: BooleanNode, scope: ExecutionScope): string {
    return dumpNode(node, node.value);
  }

  integer(node: IntegerNode, scope: ExecutionScope): any {
    return dumpNode(node, node.value);
  }

  decimal(node: DecimalNode, scope: ExecutionScope): string {
    return dumpNode(node, parseFloat(node.value));
  }

  string(node: StringNode, scope: ExecutionScope): string {
    return dumpNode(node, node.value);
  }

  addition(node: AdditionNode, scope: ExecutionScope): string {
    return dumpNode(node, [
      this.visit(node.left, scope),
      this.visit(node.right, scope),
    ]);
  }

  subtraction(node: SubtractionNode, scope: ExecutionScope): string {
    return dumpNode(node, [
      this.visit(node.left, scope),
      this.visit(node.right, scope),
    ]);
  }

  multiplication(node: MultiplicationNode, scope: ExecutionScope): string {
    return dumpNode(node, [
      this.visit(node.left, scope),
      this.visit(node.right, scope),
    ]);
  }

  division(node: DivisionNode, scope: ExecutionScope): string {
    return dumpNode(node, [
      this.visit(node.left, scope),
      this.visit(node.right, scope),
    ]);
  }

  parenthesis(node: ParenthesisNode, scope: ExecutionScope): string {
    return dumpNode(node, [this.visit(node.expression, scope)]);
  }

  negation(node: NegationNode, scope: ExecutionScope): string {
    return dumpNode(node, [this.visit(node.expression, scope)]);
  }

  declaration(node: DeclarationNode, scope: ExecutionScope): string {
    return dumpNode(node, [this.visit(node.expression, scope)]);
  }

  assignment(node: AssignmentNode, scope: ExecutionScope): any {
    return dumpNode(node, [this.visit(node.expression, scope)]);
  }

  reference(node: ReferenceNode, scope: ExecutionScope): any {
    return dumpNode(node, [node.variableName]);
  }

  block(node: BlockNode, scope: ExecutionScope): any {
    return dumpNode(
      node,
      node.children.map((node) => this.visit(node, scope)),
    );
  }

  function(node: FunctionNode, scope: ExecutionScope): any {
    return dumpNode(node, [
      ...node.arguments.map((argument) => this.visit(argument, scope)),
      this.visit(node.body, scope),
    ]);
  }

  function_call(node: FunctionCallNode, scope: ExecutionScope): any {
    return dumpNode(node, [
      ...node.arguments.map((expression) => this.visit(expression, scope)),
    ]);
  }

  program(node: ProgramNode, scope: ExecutionScope): any {
    return dumpNode(
      node,
      node.children.map((node) => this.visit(node, scope)),
    );
  }

  function_argument(node: FunctionArgumentNode, scope: ExecutionScope): any {
    return dumpNode(node, [], {
      name: node.argumentName,
      type: `${node?.type?.name}`,
    });
  }

  comparison(node: ComparisonNode, scope: ExecutionScope): any {
    return dumpNode(node, [
      this.visit(node.left, scope),
      this.visit(node.right, scope),
    ]);
  }

  if_branch(node: IfBranchNode, scope: ExecutionScope): any {
    return dumpNode(
      node,
      [
        this.visit(node.condition, scope),
        this.visit(node.body, scope),
        this.visit(node.next, scope),
      ].filter(notEmpty),
    );
  }

  for_loop(node: ForLoopNode, scope: ExecutionScope): any {
    return dumpNode(node, [
      this.visit(node.declaration, scope),
      this.visit(node.condition, scope),
      this.visit(node.iterate, scope),
      this.visit(node.body, scope),
    ]);
  }

  native_code(node: NativeCodeNode, scope: ExecutionScope): any {
    return dumpNode(node, scope);
  }

  string_argument(node: StringArgumentNode, scope: ExecutionScope): any {
    return dumpNode(node, scope);
  }

  string_template(node: StringTemplateNode, scope: ExecutionScope): any {
    return dumpNode(node, [
      ...node.arguments.map((argument) => this.visit(argument, scope)),
      ...node.parts.map((part) => this.visit(part, scope)),
    ]);
  }

  array(node: ArrayNode, scope: ExecutionScope): any {
    return dumpNode(node, [
      ...node.elements.map((element) => this.visit(element, scope)),
    ]);
  }

  array_access(node: ArrayAccessNode, scope: ExecutionScope): any {
    return dumpNode(node, scope);
  }

  return(node: ReturnNode, scope: ExecutionScope): any {
    return dumpNode(node, [this.visit(node.expression, scope)]);
  }

  type(node: BaseNode, scope: ExecutionScope): any {
    return dumpNode(node, scope);
  }
}

export function dumpAST(node: BaseNode) {
  const dumpVisitor = new DebugVisitor();
  return dumpVisitor.visit(node, new ExecutionScope());
}
