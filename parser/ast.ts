import { TokenPosition } from "typescript-parsec";
import { LogicalOperatorKinds } from "./lexer";

import { TypeInfo } from "../typechecker/type-info";

export type LiteralNodeName = "integer" | "decimal" | "string" | "boolean";

export type ExpressionNodeName =
  | "addition"
  | "array"
  | "array_access"
  | "object"
  | "object_access"
  | "string_template"
  | "string_argument"
  | "subtraction"
  | "multiplication"
  | "division"
  | "parenthesis"
  | "negation"
  | "function_call"
  | "reference"
  | "identifier"
  | "comparison"
  | "assignment"
  | "function"
  | "native_code"
  | LiteralNodeName;

export type FlowNodeName =
  | "block"
  | "declaration"
  | "if_branch"
  | "for_loop"
  | "return";

export type DeclarationNodeName = "function_argument" | "object_property";

export type TypeNodeName = "type";

export type NodeName =
  | ExpressionNodeName
  | FlowNodeName
  | DeclarationNodeName
  | TypeNodeName
  | "program";

export class BaseNode {
  name: NodeName;
  code: string;
  pos: TokenPosition;

  constructor(properties?: {}) {
    Object.assign(this, properties);
  }
}

export class ExpressionNode extends BaseNode {
  name: ExpressionNodeName;
  typeInfo?: TypeInfo;
}

export class ReturnNode extends BaseNode {
  name: "return";
  expression: ExpressionNode;
  typeInfo?: TypeInfo;
}

export class BooleanNode extends ExpressionNode {
  name: "boolean";
  value: boolean;
}

export class StringNode extends ExpressionNode {
  name: "string";
  value: string;
}

export class StringTemplateNode extends ExpressionNode {
  name: "string_template";
  value: string;
  parts: StringNode[];
  arguments: StringArgumentNode[];
}

export class ArrayNode extends ExpressionNode {
  name: "array";
  elements: ExpressionNode[];
}

export class ObjectNode extends ExpressionNode {
  name: "object";
  properties: ObjectPropertyNode[];
}

export class ObjectPropertyNode extends BaseNode {
  name: "object_property";
  propertyName: string;
  initialValue: ExpressionNode;
}

export class ObjectAccessNode extends ExpressionNode {
  name: "object_access";
  baseExpression: ExpressionNode;
  property: string;
}
export class ArrayAccessNode extends ExpressionNode {
  name: "array_access";
  indexExpression: ExpressionNode;
  baseExpression: ExpressionNode;
}

export class ComparisonNode extends ExpressionNode {
  name: "comparison";
  left: ExpressionNode;
  right: ExpressionNode;
  operator: (typeof LogicalOperatorKinds)[number];
}

export class IfBranchNode extends BaseNode {
  name: "if_branch";
  condition?: ExpressionNode;
  body: BlockNode;
  next: IfBranchNode;
}

export class ForLoopNode extends BaseNode {
  name: "for_loop";
  declaration?: DeclarationNode;
  condition?: ExpressionNode;
  iterate?: ExpressionNode;
  body: BlockNode;
}

export class NumberNode extends ExpressionNode {
  name: "decimal" | "integer";
  value: string;
}

export class IntegerNode extends NumberNode {
  name: "integer";
}

export class DecimalNode extends NumberNode {
  name: "decimal";
}

export class BinaryNode extends ExpressionNode {
  left: ExpressionNode;
  right: ExpressionNode;
}

export class AdditionNode extends BinaryNode {
  name: "addition";
}

export class SubtractionNode extends BinaryNode {
  name: "subtraction";
}

export class MultiplicationNode extends BinaryNode {
  name: "multiplication";
}

export class DivisionNode extends BinaryNode {
  name: "division";
}

export class UnaryNode extends ExpressionNode {
  expression: ExpressionNode;
}

export class ParenthesisNode extends UnaryNode {
  name: "parenthesis";
  expression: ExpressionNode;
}

export class NegationNode extends UnaryNode {
  name: "negation";
}

export class BlockNode extends BaseNode {
  name: "block";
  children: BaseNode[];
}

export class NativeCodeNode extends BaseNode {
  name: "native_code";
  callback: (...args: any[]) => any;
}

export class DeclarationNode extends BaseNode {
  name: "declaration";
  variableName: string;
  variableType: TypeInfo;
  expression: ExpressionNode;
  modifier: "const" | "var";
}

export class AssignmentNode extends ExpressionNode {
  leftHandSideExpression: ExpressionNode;
  expression: ExpressionNode;
  name: "assignment";
}

export class ReferenceNode extends ExpressionNode {
  name: "reference";
  variableName: string;
}

export class IdentifierNode extends ExpressionNode {
  name: "identifier";
  variableName: string;
}

export class FunctionArgumentNode extends BaseNode {
  name: "function_argument";
  argumentName: string;
  type?: TypeInfo;
}

export class FunctionNode extends ExpressionNode {
  name: "function";
  arguments: FunctionArgumentNode[];
  body: ExpressionNode | BlockNode | NativeCodeNode;
  returnType?: TypeInfo;
}

export class ProgramNode extends BaseNode {
  name: "program";
  children: BaseNode[];
}

export class FunctionCallNode extends ExpressionNode {
  name: "function_call";
  baseExpression: ExpressionNode;
  arguments: ExpressionNode[];
}

export class StringArgumentNode extends BaseNode {
  name: "string_argument";
  expression: ExpressionNode;
}

export class TypeNode extends BaseNode {
  name: "type";
  typeName: string;
}

export type NodeTypesMap = {
  [key in NodeName]: new () => BaseNode;
};

export const nodeTypes: NodeTypesMap = {
  addition: AdditionNode,
  block: BlockNode,
  boolean: BooleanNode,
  decimal: DecimalNode,
  division: DivisionNode,
  function: FunctionNode,
  function_call: FunctionCallNode,
  integer: IntegerNode,
  multiplication: MultiplicationNode,
  parenthesis: ParenthesisNode,
  negation: NegationNode,
  program: ProgramNode,
  string: StringNode,
  string_template: StringTemplateNode,
  string_argument: StringArgumentNode,
  subtraction: SubtractionNode,
  declaration: DeclarationNode,
  assignment: AssignmentNode,
  reference: ReferenceNode,
  identifier: IdentifierNode,
  function_argument: FunctionArgumentNode,
  comparison: ComparisonNode,
  if_branch: IfBranchNode,
  for_loop: ForLoopNode,
  native_code: NativeCodeNode,
  array: ArrayNode,
  array_access: ArrayAccessNode,
  object: ObjectNode,
  object_property: ObjectPropertyNode,
  object_access: ObjectAccessNode,
  return: ReturnNode,
  type: TypeNode,
};

export type NodeTypes = typeof nodeTypes;

export type AnyNode = (typeof nodeTypes)[NodeName];

export type Visitor<NodeTypesList extends NodeName, ContextType> = {
  [key in NodeTypesList]: (node: BaseNode, scope: ContextType) => any;
};
