import {
  buildLexer,
  Parser,
  tok,
  Token as ParsecToken,
} from "typescript-parsec";
import { TokenTypesMap } from "../lexer";

export enum StringTokenKind {
  Anything = "Anything",
  LBrace = `LBrace`,
  RBrace = `RBrace`,
  BackQuote = "BackQuote",
}

export const StringTokenRules: TokenTypesMap<
  [boolean, RegExp],
  StringTokenKind
> = {
  [StringTokenKind.LBrace]: [true, /^\{/g],
  [StringTokenKind.RBrace]: [true, /^}/g],
  [StringTokenKind.BackQuote]: [true, /^`/g],
  [StringTokenKind.Anything]: [true, /^[^{}`]*/gm],
};

export const StringToken: TokenTypesMap<
  Parser<StringTokenKind, ParsecToken<StringTokenKind>>,
  StringTokenKind
> = {
  [StringTokenKind.LBrace]: tok(StringTokenKind.LBrace),
  [StringTokenKind.RBrace]: tok(StringTokenKind.RBrace),
  [StringTokenKind.Anything]: tok(StringTokenKind.Anything),
  [StringTokenKind.BackQuote]: tok(StringTokenKind.BackQuote),
};

export const stringTemplateLexer = buildLexer(
  Object.entries(StringTokenRules).map((entry) => [
    ...entry[1],
    entry[0] as StringTokenKind,
  ]),
);
