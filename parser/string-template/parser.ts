import { alt, apply, rep_sc, rule, seq } from "typescript-parsec";
import { StringToken, StringTokenKind } from "./lexer";
import { StringArgumentNode, StringNode, StringTemplateNode } from "../ast";
import { dumpCode } from "../dump-code";

const STRING_ARGUMENT = rule<StringTokenKind, StringArgumentNode>();
const STRING_PART = rule<StringTokenKind, StringNode>();

export const STRING_TEMPLATE_CONTENT = rule<
  StringTokenKind,
  StringTemplateNode
>();

STRING_ARGUMENT.setPattern(
  apply(seq(StringToken.LBrace, STRING_PART, StringToken.RBrace), (params) => {
    const [, expression] = params;
    return {
      name: "string_argument",
      expression,
      pos: params[0].pos,
      code: dumpCode(params),
    };
  }),
);

STRING_PART.setPattern(
  apply(StringToken.Anything, (token) => {
    return {
      name: "string",
      value: token.text,
      pos: token.pos,
      code: dumpCode([token]),
    };
  }),
);

const createEmptyStringNode = (position): StringNode => ({
  name: "string",
  value: "",
  pos: position,
  code: "",
});

STRING_TEMPLATE_CONTENT.setPattern(
  apply(
    seq(
      StringToken.BackQuote,
      rep_sc(alt(STRING_ARGUMENT, STRING_PART)),
      StringToken.BackQuote,
    ),
    (params) => {
      const [, parts] = params;

      const templateNode = {
        name: "string_template",
        value: "",
        arguments: parts.filter(
          (n): n is StringArgumentNode => n.name === "string_argument",
        ),
        parts: parts.filter((n): n is StringNode => n.name === "string"),
        code: dumpCode(params),
        pos: params[0].pos,
      } as StringTemplateNode;

      // In case the string starts with argument,
      // make sure that we can zip parts and arguments later
      // by putting fake empty string at the beginning
      if (parts[0].name === "string_argument") {
        templateNode.parts = [
          createEmptyStringNode(params[0].pos),
          ...templateNode.parts,
        ];
      }

      return templateNode;
    },
  ),
);
