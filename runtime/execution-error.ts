export class ExecutionError implements Error {
  name = "EXECUTION_ERROR";
  stack: string;

  constructor(
    public message: string,
    public executionLog: any[],
  ) {
    this.stack = executionLog.join("\n");
  }
}
