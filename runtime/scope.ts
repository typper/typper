import { TokenPosition } from "typescript-parsec/lib/Lexer";

export type Variable = {
  value: any;
  type: "const" | "var";
  position: TokenPosition;
};

export class ExecutionScope {
  variables: Record<string, Variable> = {};

  constructor(private parent?: ExecutionScope) {}

  getDeclaration(name: string, deep = true) {
    return (
      this.variables[name] ??
      (deep ? this.parent?.getDeclaration(name) : undefined)
    );
  }

  isDeclared(name: string, deep = true) {
    return !!this.getDeclaration(name, deep);
  }

  declare(name: string, type: "const" | "var", position: TokenPosition) {
    if (this.isDeclared(name, false)) {
      const { row, column } = this.getDeclaration(name).position;
      throw new Error(
        `Cannot redeclare variable ${name}. Previously declared at: ${row}:${column}`,
      );
    }
    this.variables[name] = { value: undefined, type, position };
    return this.variables[name];
  }

  getVariable(name: string): any {
    if (!this.isDeclared(name)) {
      throw new Error(`Undeclared variable ${name}`);
    }
    const value = this.variables[name] ?? this.parent?.getVariable(name);
    if (value === undefined) {
      throw new Error(
        `Cannot reference variable ${name} before initialization`,
      );
    }
    return value;
  }

  setVariable(name: string, value: any) {
    const declaration = this.getDeclaration(name);
    if (!declaration) {
      throw new Error(`Undeclared variable ${name}`);
    }
    if (declaration.type === "const" && declaration.value !== undefined) {
      throw new Error(`Cannot assign read-only variable ${name}.`);
    }
    if (this.variables[name] !== undefined) {
      this.variables[name].value = value;
      return;
    }
    this.parent?.setVariable(name, value);
  }

  getLocalVariables(): Record<string, Variable> {
    return this.variables;
  }

  clear() {
    this.variables = {};
  }
}

export const deepDereference = <T extends {}>(value: T) => {
  if (Array.isArray(value)) {
    return value.map(deepDereference);
  }
  if (typeof value === "object") {
    if ("value" in value) {
      return deepDereference(value.value);
    }
    return Object.fromEntries(
      Object.entries(value).map(([key, value]) => [
        key,
        deepDereference(value),
      ]),
    );
  }
  return value;
};
