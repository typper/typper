export const globalVariables = {
  write: function () {
    process.stdout.write(`${arguments[0]}`);
  },
  print: ({ value }) => {
    console.log(value);
  },
};
