import {
  AdditionNode,
  ArrayAccessNode,
  ArrayNode,
  AssignmentNode,
  BaseNode,
  BlockNode,
  BooleanNode,
  ComparisonNode,
  DecimalNode,
  DeclarationNode,
  DivisionNode,
  ExpressionNode,
  ForLoopNode,
  FunctionArgumentNode,
  FunctionCallNode,
  FunctionNode,
  IdentifierNode,
  IfBranchNode,
  IntegerNode,
  MultiplicationNode,
  NativeCodeNode,
  NegationNode,
  NodeName,
  ObjectAccessNode,
  ObjectNode,
  ObjectPropertyNode,
  ParenthesisNode,
  ProgramNode,
  ReferenceNode,
  ReturnNode,
  StringArgumentNode,
  StringNode,
  StringTemplateNode,
  SubtractionNode,
  Visitor,
} from "../parser/ast";
import { parseExpression, parseProgram } from "../parser/parser";
import { ExecutionScope } from "./scope";
import { TokenKind } from "../parser/lexer";
import { colorCode } from "../parser/debug/print-error";
import { extractByPositionRange, TokenPosition } from "typescript-parsec";
import { globalVariables } from "./std/io";
import { zip } from "../ts-utils/zip";
import { checkTypes } from "../typechecker/type-checker";
import { ExecutionError } from "./execution-error";

export class ReturnValue {
  constructor(public value: any) {}
}

export function mkConst<T>(value: T) {
  return { value, type: "const" };
}

export function mkVar<T>(value: T) {
  return { value, type: "var" };
}

export class ExecutionVisitor implements Visitor<NodeName, ExecutionScope> {
  private executionLog = [];

  visit<T extends BaseNode>(node: T, scope: ExecutionScope) {
    if (!node) {
      return null;
    }
    if (!this[node.name]) {
      throw new ExecutionError(
        `Unknown AST node type: ${node.name}`,
        this.executionLog,
      );
    }
    const result = this[node.name](node as any, scope);
    this.executionLog.push({ node, scope, result });
    return result;
  }

  identifier(identifier: IdentifierNode, scope: ExecutionScope) {
    return scope.getVariable(identifier.variableName);
  }

  object(node: ObjectNode, scope: ExecutionScope) {
    const result = {};
    for (const property of node.properties) {
      result[property.propertyName] = this.visit(property, scope);
    }
    return mkConst(result);
  }

  object_property(node: ObjectPropertyNode, scope: ExecutionScope) {
    return this.visit(node.initialValue, scope);
  }

  object_access(node: ObjectAccessNode, scope: ExecutionScope) {
    const object = this.visit(node.baseExpression, scope);
    const propertyName = node.property;
    return object.value[propertyName];
  }

  boolean(node: BooleanNode, scope: ExecutionScope) {
    return mkConst(node.value);
  }

  integer(node: IntegerNode, scope: ExecutionScope): any {
    return mkConst(parseInt(node.value));
  }

  decimal(node: DecimalNode, scope: ExecutionScope): any {
    return mkConst(parseFloat(node.value));
  }

  string(node: StringNode, scope: ExecutionScope) {
    return mkConst(node.value);
  }

  addition(node: AdditionNode, scope: ExecutionScope) {
    return mkConst(
      this.visit(node.left, scope).value + this.visit(node.right, scope).value,
    );
  }

  subtraction(node: SubtractionNode, scope: ExecutionScope) {
    return mkConst(
      this.visit(node.left, scope).value - this.visit(node.right, scope).value,
    );
  }
  multiplication(node: MultiplicationNode, scope: ExecutionScope) {
    return mkConst(
      this.visit(node.left, scope).value * this.visit(node.right, scope).value,
    );
  }
  division(node: DivisionNode, scope: ExecutionScope) {
    return mkConst(
      this.visit(node.left, scope).value / this.visit(node.right, scope).value,
    );
  }
  parenthesis(node: ParenthesisNode, scope: ExecutionScope): any {
    return this.visit(node.expression, scope);
  }

  negation(node: NegationNode, scope: ExecutionScope): any {
    return mkConst(-this.visit(node.expression, scope).value);
  }

  declaration(node: DeclarationNode, scope: ExecutionScope): any {
    const declaredValue = scope.declare(
      node.variableName,
      node.modifier,
      node.pos,
    );
    scope.setVariable(
      node.variableName,
      this.visit(node.expression, scope).value,
    );
    return declaredValue;
  }

  assignment(node: AssignmentNode, scope: ExecutionScope): any {
    const lhsValue = this.visit(node.leftHandSideExpression, scope);
    if (lhsValue.type === "const") {
      throw new ExecutionError("Cannot assign to const", this.executionLog);
    }
    const expressionValue = this.visit(node.expression, scope);
    lhsValue.value = expressionValue.value;
    return lhsValue;
  }

  reference(node: ReferenceNode, scope: ExecutionScope): any {
    return scope.getVariable(node.variableName);
  }

  block(node: BlockNode, scope: ExecutionScope): any {
    const blockContext = new ExecutionScope(scope);
    const result = node.children.map((node) => this.visit(node, blockContext));
    return result[result.length - 1];
  }

  function(node: FunctionNode, scope: ExecutionScope): any {
    return mkConst(node);
  }

  function_call(node: FunctionCallNode, scope: ExecutionScope): any {
    const functionNode = this.visit(node.baseExpression, scope).value;
    const functionScope = new ExecutionScope(scope);
    const argumentValues = node.arguments.map((expression) =>
      this.visit(expression, scope),
    );

    functionNode.arguments.forEach((argument, index) => {
      functionScope.declare(argument.argumentName, "const", argument.pos);
      functionScope.setVariable(
        argument.argumentName,
        argumentValues[index].value,
      );
    });
    functionScope.declare("arguments", "const", functionNode.pos);
    functionScope.setVariable("arguments", argumentValues);
    try {
      const result = this.visit(functionNode.body, functionScope);
      return result;
    } catch (thrown) {
      if (thrown instanceof ReturnValue) {
        return thrown.value;
      }
      throw thrown;
    }
  }

  program(node: ProgramNode, scope: ExecutionScope): any {
    return node.children.map((node) => this.visit(node, scope));
  }

  function_argument(node: FunctionArgumentNode, scope: ExecutionScope): any {}

  comparison(node: ComparisonNode, scope: ExecutionScope): any {
    const operators = {
      [TokenKind.And]: (a, b) => a && b,
      [TokenKind.Or]: (a, b) => a || b,

      [TokenKind.Equal]: (a, b) => a === b,
      [TokenKind.NotEqual]: (a, b) => a !== b,

      [TokenKind.Greater]: (a, b) => a > b,
      [TokenKind.GreaterEqual]: (a, b) => a >= b,
      [TokenKind.Less]: (a, b) => a < b,
      [TokenKind.LessEqual]: (a, b) => a <= b,
    };
    const left = this.visit(node.left, scope);
    const right = this.visit(node.right, scope);

    const result = operators[node.operator](left.value, right.value);
    return mkConst(result);
  }

  if_branch(node: IfBranchNode, scope: ExecutionScope): any {
    if (
      !node.condition ||
      (node.condition && this.visit(node.condition, scope).value)
    ) {
      return this.visit(node.body, scope);
    }
    return node.next ? this.visit(node.next, scope) : undefined;
  }

  for_loop(node: ForLoopNode, scope: ExecutionScope): any {
    const loopScope = new ExecutionScope(scope);
    this.visit(node.declaration, loopScope);
    do {
      this.visit(node.body, loopScope);
      this.visit(node.iterate, loopScope);
    } while (this.visit(node.condition, loopScope).value);
  }

  native_code(node: NativeCodeNode, scope: ExecutionScope): any {
    console.log(scope.getVariable("arguments"));
    return node.callback.apply(
      node.callback,
      scope.getVariable("arguments").value,
    );
  }

  string_template(node: StringTemplateNode, scope: ExecutionScope): any {
    const argumentValues = node.arguments.map(
      ({ expression }) => this.visit(expression, scope).value,
    );
    return mkConst(
      zip(
        node.parts.map((part) => this.visit(part, scope).value),
        argumentValues,
      ).join(""),
    );
  }

  string_argument(node: StringArgumentNode, scope: ExecutionScope): any {
    const result = this.visit(node.expression, scope);
    return result.value;
  }

  array(node: ArrayNode, scope: ExecutionScope): any {
    const elements = node.elements
      .map((element) => this.visit(element, scope).value)
      .map((value) => mkVar(value));
    return mkConst(elements);
  }

  array_access(node: ArrayAccessNode, scope: ExecutionScope): any {
    const arrayValue = this.visit(node.baseExpression, scope);
    const indexValue = this.visit(node.indexExpression, scope);
    return arrayValue.value[indexValue.value];
  }

  return(node: ReturnNode, scope: ExecutionScope): never {
    const value = this.visit(node.expression, scope);
    throw new ReturnValue(value);
  }

  type(node: BaseNode, scope: ExecutionScope): any {
    // empty - used only in type checker
  }

  dumpExecutionLog() {
    console.log(this.executionLog);
  }
}

export function executeSyntaxTree(
  node: ProgramNode | ExpressionNode,
  globalContext = new ExecutionScope(),
): any {
  const executor = new ExecutionVisitor();
  try {
    const result = executor.visit(node, globalContext);
    // executor.dumpExecutionLog();
    return result;
  } catch (e) {
    if (e instanceof ExecutionError) {
      console.error(e);
    }
    throw e;
  }
}

function createNativeFunction(callback: (...args: any[]) => any): FunctionNode {
  return {
    name: "function",
    code: callback.toString(),
    arguments: [],
    pos: {
      rowEnd: 0,
      rowBegin: 0,
      columnEnd: 0,
      columnBegin: 0,
      index: 0,
    },
    body: {
      name: "native_code",
      pos: {
        rowEnd: 0,
        rowBegin: 0,
        columnEnd: 0,
        columnBegin: 0,
        index: 0,
      },
      callback,
      code: callback.toString(),
    },
  };
}

function initGlobalContext(globalContext: ExecutionScope) {
  Object.entries(globalVariables).forEach(([name, value]) => {
    if (globalContext.isDeclared(name)) {
      return;
    }
    globalContext.declare(name, "const", {
      rowBegin: 0,
      columnBegin: 0,
      columnEnd: 0,
      rowEnd: 0,
      index: 0,
    });
    globalContext.setVariable(name, createNativeFunction(value));
  });
}

export function executeProgram(
  program: string,
  globalContext = new ExecutionScope(),
): any {
  initGlobalContext(globalContext);
  try {
    const syntaxTree = parseProgram(program);
    checkTypes(syntaxTree);
    const result = executeSyntaxTree(syntaxTree, globalContext);
    return result.at(-1)?.value;
  } catch (e) {
    if (e.pos) {
      const pos2: TokenPosition = {
        rowBegin: e.pos.rowEnd,
        columnBegin: e.pos.columnEnd,
        rowEnd: e.pos.rowEnd + 1,
        columnEnd: e.pos.columnEnd + 1,
        index: e.pos.index + 1,
      };
      const text = extractByPositionRange(program, e.pos, pos2);
      console.error(
        e.message
          ? e.message
          : `Unexpected token "${text}" at (${e.pos.rowBegin}, ${e.pos.columnBegin}):`,
      );
      console.log();
      console.log(e);
      console.log();
      console.log(colorCode(program, e.pos), "\n");
    } else {
      console.log(e);
      throw e;
    }
  }
}

export function executeExpression(
  expression: string,
  globalContext = new ExecutionScope(),
) {
  const syntaxTree = parseExpression(expression);
  checkTypes(syntaxTree); // FIXME: return new syntax tree from here instead of mutating
  return executeSyntaxTree(syntaxTree, globalContext).value;
}
