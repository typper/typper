import { readFile } from "node:fs";
import { executeProgram } from "../runtime/run";

const fileName = process.argv[2];
readFile(fileName, "utf8", (error, fileContent) => {
  if (error) {
    console.error(error);
    return;
  }
  const result = executeProgram(fileContent);
  console.log("\n=> ", result);
});
