import { readFile } from "node:fs";
import { compileProgram } from "../compiler/compiler";

const fileName = process.argv[2];
readFile(fileName, "utf8", (error, fileContent) => {
  if (error) {
    console.error(error);
    return;
  }
  const compiled = compileProgram(fileContent);
  console.log(compiled);
});
