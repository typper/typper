import * as readline from "node:readline";
import { ExecutionScope } from "../runtime/scope";
import { executeProgram } from "../runtime/run";

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: true,
});
const languageContext = new ExecutionScope();

rl.setPrompt("Typper => ");
rl.prompt();

rl.on("line", (line) => {
  if (!line) {
    return;
  }
  try {
    const result = executeProgram(line, languageContext);
    console.log(`=> ${result}`);
  } catch (error) {
    console.log(error);
  }
  rl.prompt();
});
