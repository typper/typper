### What is Typper?

Typper aims to be a simple programming language with a very strong type system,
that would allow extending with custom DLSs (Domain Specific Languages).

For example, it could be used to create an ORM with native support for SQL:

```typescript
const notExistingTable = DB<Schema>(`SELECT * FROM does_not_exist`); // compilation error - table doesn't exist
const users = DB<Schema>(`SELECT firstName, lastName, doesNotExist FROM users WHERE `) // compilation error - column doesn't exist

const posts = DB<Schema>(`
    SELECT CONCAT(firstName, ' ', lastName) as authorName, p.*
    FROM posts p LEFT JOIN users u ON u.id = p.author_id
`);
// typeof posts: {authorName: string, id: int, title: string, createdAt: Date, }

```
Or GraphQL queries:

```typescript
// typeof result = {name: string, friends: {name:string}[]}
const result = GQL.query(`{
  hero {
    name
    friends {
      name
    }
  }
}
`);
```

Or HTML elements:
```typescript
const invalidHTML = HTML(`<html><h1><div></h1></html>`); // compilation error: unclosed tag <div> at (x, y)
const invalidHTML = HTML(`<html><h1><div></div></h1></html>`); // compilation error: <div> cannot be nested in <h1>
const validHTML = HTML(`<html><h1 id="mainHeader"></h1></html>`); // typeof validHTML = HtmlElement({children: [H1Element({id: "mainHeader"})]})
validHTML.byId('doesNotExist'); // compilation error
```

Or CSS:

```typescript
const styles = CSS(`.mainHeader { color: green; }, .label { color: red; }`);
// typeof styles = { mainHeader: { color: 'green' }, label: { color: 'red' } }
```



## Current state

Currently basic expressions (arithmetical, logical, function delegates, function calls, strings) and statements (loops and conditions)
are supported. There is no type system yet at all.

## Example syntax
Some examples can be found in `examples` folder. Here is a short snippet of how syntax currently looks like.
The syntax is certainly not finalized and subject to change.

```typescript
const fib = n: int => {
    var a = 0;
    var b = 1;
    var result = 1;
    if (n == 0) {
        result = 0;
    } else {
        for (var i = 2; i <= n; i = i + 1) {
            result = a + b;
            a = b;
            b = result;
        }
    }
     result;
};
const x = fib(6);
print(`FIB(6) = {x}`);
```