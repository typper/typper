import { TokenPosition } from "typescript-parsec";
import { BaseNode } from "../../parser/ast";
import { TypeInfo } from "../type-info";

export class InvalidTypeError extends Error {
  public name = "INVALID_TYPE_ERROR";
  public pos: TokenPosition;
  public nodeName: string;

  constructor(
    public message: string,
    left: TypeInfo,
    right: TypeInfo,
    node: BaseNode,
  ) {
    super();
    this.pos = node.pos || left.position;
    this.nodeName = node.name;
  }
}
