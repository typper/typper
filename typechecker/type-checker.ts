import {
  AdditionNode,
  ArrayAccessNode,
  ArrayNode,
  AssignmentNode,
  BaseNode,
  BlockNode,
  BooleanNode,
  ComparisonNode,
  DecimalNode,
  DeclarationNode,
  DivisionNode,
  ExpressionNode,
  ForLoopNode,
  FunctionArgumentNode,
  FunctionCallNode,
  FunctionNode,
  IdentifierNode,
  IfBranchNode,
  IntegerNode,
  MultiplicationNode,
  NativeCodeNode,
  NegationNode,
  NodeName,
  ObjectAccessNode,
  ObjectNode,
  ObjectPropertyNode,
  ParenthesisNode,
  ProgramNode,
  ReferenceNode,
  ReturnNode,
  StringArgumentNode,
  StringNode,
  StringTemplateNode,
  SubtractionNode,
  Visitor,
} from "../parser/ast";
import { TokenKind } from "../parser/lexer";
import { zip } from "../ts-utils/zip";
import { TypeScope } from "./scope";
import { globalVariables } from "../runtime/std/io";
import {
  createBoolType,
  createFunctionType,
  createLengthType,
  Type,
} from "./types";
import { InvalidTypeError } from "./errors/invalid-type-error";
import { ObjectTypeInfo, TypeInfo } from "./type-info";

function getTypeDisplayName(type: TypeInfo) {
  if (!type) return "unknown";
  return type.typeArguments.length > 0
    ? `${type.name}[${type.typeArguments.map(getTypeDisplayName)}]`
    : type.name;
}

export class TypeCheckerVisitor implements Visitor<NodeName, TypeScope> {
  public executionLog = [];

  visit<T extends BaseNode>(node: T, scope: TypeScope): TypeInfo {
    if (!node) {
      return null;
    }
    const result = this[node.name](node as any, scope);
    this.executionLog.push({ node, variables: scope.variables, result });
    return result;
  }

  identifier(node: IdentifierNode, scope: TypeScope): TypeInfo {
    return node.typeInfo || scope.getType(node.variableName);
  }

  object(node: ObjectNode, scope: TypeScope) {
    const properties = node.properties.map((property) => [
      property.propertyName,
      this.visit(property, scope),
    ]);
    return new ObjectTypeInfo(Object.fromEntries(properties), [], node.pos); // FIXME: add property types information
  }

  object_property(node: ObjectPropertyNode, scope: TypeScope) {
    return this.visit(node.initialValue, scope);
  }

  object_access(node: ObjectAccessNode, scope: TypeScope) {
    const objectType = this.visit(node.baseExpression, scope);
    if (!isObjectType(objectType)) {
      throw new InvalidTypeError(
        "Object access invalid - object expression is not an object",
        Type.object,
        objectType,
        node.baseExpression,
      );
    }
    return objectType.propertyTypes[node.property];
  }

  boolean(node: BooleanNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = TypeInfo.create("bool", [], node.pos));
  }

  integer(node: IntegerNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = TypeInfo.create("int", [], node.pos));
  }

  decimal(node: DecimalNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = TypeInfo.create("double", [], node.pos));
  }

  string(node: StringNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = TypeInfo.create("string", [], node.pos));
  }

  addition(node: AdditionNode, scope: TypeScope): TypeInfo {
    node.typeInfo = this.checkExpressionTypes(node.left, node.right, scope);
    return node.typeInfo;
  }

  subtraction(node: SubtractionNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = this.checkExpressionTypes(
      node.left,
      node.right,
      scope,
    ));
  }

  multiplication(node: MultiplicationNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = this.checkExpressionTypes(
      node.left,
      node.right,
      scope,
    ));
  }

  division(node: DivisionNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = this.checkExpressionTypes(
      node.left,
      node.right,
      scope,
    ));
  }

  parenthesis(node: ParenthesisNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = this.visit(node.expression, scope));
  }

  negation(node: NegationNode, scope: TypeScope): TypeInfo {
    return (node.typeInfo = this.visit(node.expression, scope));
  }

  declaration(node: DeclarationNode, scope: TypeScope): TypeInfo {
    const expressionType = this.visit(node.expression, scope);
    if (node.variableType && !matchTypes(node.variableType, expressionType)) {
      throw new InvalidTypeError(
        `Wrong types: ${getTypeDisplayName(
          node.variableType,
        )} ${getTypeDisplayName(expressionType)}`,
        node.variableType,
        expressionType,
        node,
      );
    }

    node.variableType = expressionType;

    scope.declare(node.variableName, expressionType);

    return node.variableType;
  }

  assignment(node: AssignmentNode, scope: TypeScope): TypeInfo {
    const expressionType = this.visit(node.expression, scope);
    const variableType = this.visit(node.leftHandSideExpression, scope);

    if (variableType.name !== expressionType.name) {
      throw new InvalidTypeError(
        `Wrong types: ${variableType.name} ${expressionType.name}`,
        variableType,
        expressionType,
        node,
      );
    }

    return (node.typeInfo = expressionType);
  }

  reference(node: ReferenceNode, scope: TypeScope): TypeInfo {
    const variableType = scope.getType(node.variableName);
    return (node.typeInfo = variableType);
  }

  block(parentNode: BlockNode, scope: TypeScope): TypeInfo {
    const result = parentNode.children.map((node) => this.visit(node, scope));
    return result.pop();
  }

  function(node: FunctionNode, scope: TypeScope): TypeInfo {
    const typeInfo = TypeInfo.create("fn", [], node.pos);
    const functionScope = new TypeScope(
      scope,
      `fn[${node.arguments.map((a) => a.argumentName).join(",")}]`,
    );
    const argumentTypes = node.arguments.map((argument) => {
      const argumentType = this.visit(argument, scope);
      functionScope.declare(argument.argumentName, argumentType);
      return argumentType;
    });

    if (node.returnType) {
      functionScope.declare("#return#", node.returnType);
    }

    this.visit(node.body, functionScope);

    node.returnType = functionScope.getType("#return#", false);

    typeInfo.typeArguments = [...argumentTypes, node.returnType];
    return typeInfo;
  }

  function_call(node: FunctionCallNode, scope: TypeScope): any {
    const typeInfo: TypeInfo = this.visit(node.baseExpression, scope);

    if (typeInfo?.name !== "fn") {
      throw new InvalidTypeError(
        `${node.baseExpression} (${typeInfo?.name}) is not a valid function`,
        typeInfo,
        createFunctionType([], createLengthType(1), node.pos),
        node,
      );
    }

    const callArgumentTypes = node.arguments.map((expression) =>
      this.visit(expression, scope),
    );

    const callArguments = typeInfo?.typeArguments || [];
    callArguments
      .slice(0, callArguments.length - 1) // last type argument is a return type
      .forEach((argumentType, index) => {
        const argumentTypeName = callArgumentTypes[index].name;
        const expectedArgumentTypeName = argumentType?.name;
        if (argumentTypeName !== expectedArgumentTypeName) {
          throw new InvalidTypeError(
            `Invalid argument type for argument at index "${index}" of "${node.baseExpression.code}" call. Expected: ${expectedArgumentTypeName}, got: ${argumentTypeName}`,
            argumentType,
            callArgumentTypes[index],
            node,
          );
        }
      });

    return (
      typeInfo.typeArguments[typeInfo.typeArguments.length - 1] ||
      TypeInfo.create("any", [], node.pos)
    );
  }

  program(node: ProgramNode, scope: TypeScope): any {
    return node.children.map((node) => this.visit(node, scope));
  }

  function_argument(node: FunctionArgumentNode, scope: TypeScope): TypeInfo {
    return node.type;
  }

  comparison(node: ComparisonNode, scope: TypeScope): TypeInfo {
    const leftType = this.visit(node.left, scope);
    const rightType = this.visit(node.right, scope);

    if (!matchTypes(leftType, rightType)) {
      throw new InvalidTypeError(
        `Wrong types: ${getTypeDisplayName(leftType)} ${getTypeDisplayName(
          rightType,
        )}`,
        leftType,
        rightType,
        node,
      );
    }

    const operators: {
      [key: string]: (type: string) => boolean;
    } = {
      [TokenKind.And]: (type) => type === "bool",
      [TokenKind.Or]: (type) => type === "bool",

      [TokenKind.Equal]: () => true,
      [TokenKind.NotEqual]: (type) => true,

      [TokenKind.Greater]: (type) => type === "int" || type === "string",
      [TokenKind.GreaterEqual]: (type) => type === "int" || type === "string",
      [TokenKind.Less]: (type) => type === "int" || type === "string",
      [TokenKind.LessEqual]: (type) => type === "int" || type === "string",
    };

    if (!operators[node.operator](leftType.name as string)) {
      throw new InvalidTypeError(
        `Invalid type ${leftType.name} for operator ${node.operator}`,
        leftType,
        rightType,
        node,
      );
    }

    return TypeInfo.create("bool", [], node.pos);
  }

  if_branch(node: IfBranchNode, scope: TypeScope): any {
    if (
      !node.condition ||
      (node.condition && this.visit(node.condition, scope))
    ) {
      return this.visit(node.body, scope);
    }
    return node.next ? this.visit(node.next, scope) : undefined;
  }

  for_loop(node: ForLoopNode, scope: TypeScope): any {
    this.visit(node.declaration, scope);
    this.visit(node.body, scope);
    this.visit(node.iterate, scope);
    const conditionType = this.visit(node.condition, scope);
    if (conditionType.name !== "bool") {
      throw new InvalidTypeError(
        `Invalid type ${conditionType.name} for loop condition expression "${node.condition.code}"`,
        conditionType,
        createBoolType(),
        node,
      );
    }
  }

  native_code(node: NativeCodeNode, scope: TypeScope): any {
    return "fn";
  }

  string_template(node: StringTemplateNode, scope: TypeScope): any {
    const argumentValues = node.arguments.map((expression) =>
      this.visit(expression, scope),
    );
    return zip(
      node.parts.map((part) => this.visit(part, scope)),
      argumentValues,
    ).join("");
  }

  string_argument(node: StringArgumentNode, scope: TypeScope): any {
    return this.visit(node.expression, scope);
  }

  array(node: ArrayNode, scope: TypeScope): TypeInfo {
    if (node.elements.length > 0) {
      node.typeInfo.typeArguments[0] = this.visit(node.elements[0], scope);
    }
    return node.typeInfo;
  }

  array_access(node: ArrayAccessNode, scope: TypeScope): any {
    const valueType = this.visit(node.baseExpression, scope);
    const indexType = this.visit(node.indexExpression, scope);

    if (valueType.name !== "array") {
      throw new InvalidTypeError(
        `Value of "${node.baseExpression.code}" is not an array. Actual type: "${valueType.name}"`,
        valueType,
        indexType,
        node,
      );
    }
    if (indexType.name !== "int") {
      throw new InvalidTypeError(
        `Index type ${node.indexExpression.code} is not a number. Actual type: ${indexType.name}"`,
        valueType,
        indexType,
        node,
      );
    }

    return (node.typeInfo = valueType.typeArguments[0]);
  }

  return(node: ReturnNode, scope: TypeScope): any {
    const returnType = scope.getType("#return#");
    node.typeInfo = this.visit(node.expression, scope);
    if (returnType) {
      if (!matchTypes(node.typeInfo, returnType)) {
        throw new InvalidTypeError(
          `Return type ${node.typeInfo.name} is not compatible with previously declared type. Previous: ${returnType.name}, declared at ${returnType.position}`,
          returnType,
          node.typeInfo,
          node,
        );
      }
    }
    scope.declare("#return#", node.typeInfo);
    return node.typeInfo;
  }

  type(node: BaseNode, scope: TypeScope): any {
    // empty - used only in type checker
  }

  private checkExpressionTypes(
    left: ExpressionNode,
    right: ExpressionNode,
    scope: TypeScope,
  ) {
    left.typeInfo = this.visit(left, scope);
    right.typeInfo = this.visit(right, scope);

    if (!matchTypes(left.typeInfo, right.typeInfo)) {
      throw new InvalidTypeError(
        `Wrong types: ${getTypeDisplayName(left.typeInfo)} ${getTypeDisplayName(
          right.typeInfo,
        )}`,
        left.typeInfo,
        right.typeInfo,
        left,
      );
    }

    return left.typeInfo;
  }
}

function initGlobalContext(globalTypes: TypeScope) {
  Object.entries(globalVariables).forEach((variable) => {
    globalTypes.declare(
      variable[0],
      TypeInfo.create("fn", [], {
        rowBegin: 0,
        rowEnd: 0,
        columnBegin: 0,
        columnEnd: 0,
        index: 0,
      }),
    );
  });
}
export function checkTypes(
  node: ProgramNode | ExpressionNode,
  globalTypes = new TypeScope(undefined, "global"),
): TypeInfo {
  const executor = new TypeCheckerVisitor();
  initGlobalContext(globalTypes);
  try {
    return executor.visit(node, globalTypes);
  } catch (e) {
    throw e;
  }
}

export function matchTypes(left: TypeInfo, right: TypeInfo): boolean {
  if (left === right) {
    return true;
  }
  if (
    left?.name !== right?.name ||
    left?.typeArguments.length !== right.typeArguments.length
  ) {
    return false;
  }
  return left.typeArguments.every((left, index) =>
    matchTypes(left, right.typeArguments[index]),
  );
}

export function isObjectType(typeInfo: TypeInfo): typeInfo is ObjectTypeInfo {
  return typeInfo.name === "object";
}
