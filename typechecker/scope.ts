import { matchTypes } from "./type-checker";
import { BaseNode } from "../parser/ast";
import { InvalidTypeError } from "./errors/invalid-type-error";
import { TypeInfo } from "./type-info";

export class TypeScope {
  variables: Record<string, TypeInfo> = {};

  constructor(
    private parent?: TypeScope,
    private name: string = "unknown",
  ) {}

  getType(name: string, deep = true): TypeInfo {
    return (
      this.variables[name] ?? (deep ? this.parent?.getType(name) : undefined)
    );
  }

  isTyped(name: string, deep = true) {
    return !!this.getType(name, deep);
  }

  declare(name: string, type: TypeInfo) {
    const oldType = this.getType(name);
    if (
      type !== undefined &&
      this.isTyped(name, true) &&
      !matchTypes(type, oldType)
    ) {
      const { rowBegin, columnBegin } = oldType.position;
      throw new InvalidTypeError(
        `Cannot redeclare type (${type?.name}) for ${name}. Previously declared at: ${rowBegin}:${columnBegin} as: ${oldType.name}`,
        type,
        oldType,
        { name, pos: this.getType(name).position, code: "" } as BaseNode, // FIXME: Better type
      );
    }
    this.variables[name] = type;
  }

  clear() {
    this.variables = {};
  }
}
