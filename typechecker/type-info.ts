import { TokenPosition } from "typescript-parsec/lib/Lexer";
import { TypeName } from "./basic-types";

export class TypeInfo {
  constructor(
    public name: string | number,
    public typeArguments: TypeInfo[],
    public position: TokenPosition,
  ) {}

  static create = (
    name: TypeName | number,
    typeArguments: TypeInfo[],
    position: TokenPosition,
  ): TypeInfo => new TypeInfo(name, typeArguments, position);
}

export type PropertyTypes = { [key: string]: TypeInfo };

export class ObjectTypeInfo extends TypeInfo {
  constructor(
    public propertyTypes: PropertyTypes,
    typeArguments: TypeInfo[],
    position: TokenPosition,
  ) {
    super("object", typeArguments, position);
  }
}
