import { TokenPosition } from "typescript-parsec";
import { TypeInfo } from "./type-info";

export function createArrayType(
  elementType: TypeInfo,
  length?: number,
  position?: TokenPosition,
) {
  const typeArguments = [elementType];
  if (length) {
    typeArguments.push(createLengthType(length, position));
  }
  return TypeInfo.create("array", typeArguments, position);
}

export function createFunctionType(
  argumentTypes: TypeInfo[],
  returnType: TypeInfo,
  position?: TokenPosition,
) {
  const typeArguments = [...argumentTypes, returnType];
  return TypeInfo.create("fn", typeArguments, position);
}

export function createBoolType(position?: TokenPosition) {
  return TypeInfo.create("bool", [], position);
}

export function createLengthType(length: number, position?: TokenPosition) {
  return TypeInfo.create(
    "length",
    [TypeInfo.create(length, [], position)],
    position,
  );
}

const zeroPosition: TokenPosition = {
  columnBegin: 0,
  columnEnd: 0,
  index: 0,
  rowBegin: 0,
  rowEnd: 0,
};

export const Type = {
  int: TypeInfo.create("int", [], zeroPosition),
  long: TypeInfo.create("long", [], zeroPosition),
  float: TypeInfo.create("float", [], zeroPosition),
  double: TypeInfo.create("double", [], zeroPosition),
  bool: TypeInfo.create("bool", [], zeroPosition),
  string: TypeInfo.create("string", [], zeroPosition),
  object: TypeInfo.create("object", [], zeroPosition),
};
