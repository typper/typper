export type AnyType = "any" | string;
export type NumberType = "int" | "long" | "float" | "double" | "length";
export type TypeName =
  | NumberType
  | "string"
  | "bool"
  | "fn"
  | "array"
  | AnyType;
