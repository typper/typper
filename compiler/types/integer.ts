import { IRBuilder, Value } from "../../packages/llvm-bindings";
import { BaseIRBuilder } from "./base";

export class IntegerIRBuilder extends BaseIRBuilder {
  constructor(builder: IRBuilder) {
    super(builder);
  }

  add(left: Value, right: Value) {
    return this.builder.CreateAdd(left, right);
  }
  sub(left: Value, right: Value) {
    return this.builder.CreateSub(left, right);
  }
  mul(left: Value, right: Value) {
    return this.builder.CreateMul(left, right);
  }
  div(left: Value, right: Value) {
    return this.builder.CreateSDiv(left, right);
  }
}
