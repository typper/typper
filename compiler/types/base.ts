import { IRBuilder, Value } from "../../packages/llvm-bindings";

export abstract class BaseIRBuilder {
  protected constructor(protected builder: IRBuilder) {}

  abstract add(left: Value, right: Value);
  abstract sub(left: Value, right: Value);
  abstract mul(left: Value, right: Value);
  abstract div(left: Value, right: Value);
}
