import { IRBuilder } from "../../packages/llvm-bindings";
import { TypeInfo } from "../../typechecker/type-info";
import { IntegerIRBuilder } from "./integer";
import { DoubleIRBuilder } from "./double";

export class TypeIRBuilderFactory {
  constructor(private builder: IRBuilder) {}
  getIRBuilderForType(type: TypeInfo) {
    if (type.name === "int") {
      return new IntegerIRBuilder(this.builder);
    }
    if (type.name === "double") {
      return new DoubleIRBuilder(this.builder);
    }
  }
}
