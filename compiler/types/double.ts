import { IRBuilder, Value } from "../../packages/llvm-bindings";
import { BaseIRBuilder } from "./base";

export class DoubleIRBuilder extends BaseIRBuilder {
  constructor(builder: IRBuilder) {
    super(builder);
  }
  add(left: Value, right: Value) {
    return this.builder.CreateFAdd(left, right);
  }
  sub(left: Value, right: Value) {
    return this.builder.CreateFSub(left, right);
  }
  mul(left: Value, right: Value) {
    return this.builder.CreateFMul(left, right);
  }
  div(left: Value, right: Value) {
    return this.builder.CreateFDiv(left, right);
  }
  literal(node: Value) {
    this.builder.getDoubleTy();
  }
}
