import {
  AdditionNode,
  BaseNode,
  BooleanNode,
  DecimalNode,
  DivisionNode,
  FunctionCallNode,
  FunctionNode,
  IntegerNode,
  MultiplicationNode,
  NodeName,
  ProgramNode,
  SubtractionNode,
  Visitor,
} from "../parser/ast";
import { CompilationContext } from "./context";
import { ExecutionError } from "../runtime/execution-error";
import { parseProgram } from "../parser/parser";
import { checkTypes } from "../typechecker/type-checker";
import {
  BasicBlock,
  ConstantFP,
  ConstantInt,
  Function,
  FunctionType,
  IRBuilder,
  LLVMContext,
  Module,
  verifyFunction,
  verifyModule,
  WriteBitcodeToFile,
} from "../packages/llvm-bindings";
import { TypeIRBuilderFactory } from "./types/factory";

class CompilerVisitor implements Visitor<NodeName, CompilationContext> {
  private context = new LLVMContext();
  private module = new Module("program", this.context);
  private builder = new IRBuilder(this.context);
  private typeFactory = new TypeIRBuilderFactory(this.builder);
  private compilationLog = [];

  constructor() {}

  string(node: BaseNode, scope: CompilationContext) {}

  object(node: BaseNode, scope: CompilationContext) {}

  function(node: BaseNode, scope: CompilationContext) {}

  array(node: BaseNode, scope: CompilationContext) {}

  array_access(node: BaseNode, scope: CompilationContext) {}

  object_access(node: BaseNode, scope: CompilationContext) {}

  string_template(node: BaseNode, scope: CompilationContext) {}

  string_argument(node: BaseNode, scope: CompilationContext) {}

  parenthesis(node: BaseNode, scope: CompilationContext) {}

  negation(node: BaseNode, scope: CompilationContext) {}

  function_call(node: FunctionCallNode, scope: CompilationContext) {}

  reference(node: BaseNode, scope: CompilationContext) {}

  identifier(node: BaseNode, scope: CompilationContext) {}

  comparison(node: BaseNode, scope: CompilationContext) {}

  assignment(node: BaseNode, scope: CompilationContext) {}

  native_code(node: BaseNode, scope: CompilationContext) {}

  block(node: BaseNode, scope: CompilationContext) {}

  declaration(node: FunctionNode, scope: CompilationContext) {
    const returnType = this.builder.getInt64Ty();
    const paramTypes = [this.builder.getInt64Ty(), this.builder.getInt64Ty()];
    const functionType = FunctionType.get(returnType, paramTypes, false);
    const func = Function.Create(
      functionType,
      Function.LinkageTypes.ExternalLinkage,
      node.name,
      this.module,
    );

    const entryBB = BasicBlock.Create(this.context, "entry", func);
    this.builder.SetInsertPoint(entryBB);
    const a = func.getArg(0);
    const b = func.getArg(1);
    const result = this.builder.CreateAdd(a, b);
    this.builder.CreateRet(result);
  }

  if_branch(node: BaseNode, scope: CompilationContext) {}

  for_loop(node: BaseNode, scope: CompilationContext) {}

  return(node: BaseNode, scope: CompilationContext) {}

  function_argument(node: BaseNode, scope: CompilationContext) {}

  object_property(node: BaseNode, scope: CompilationContext) {}

  type(node: BaseNode, scope: CompilationContext) {}

  integer(node: IntegerNode, scope: CompilationContext) {
    return this.builder.getInt64(parseInt(node.value));
  }
  decimal(node: DecimalNode, scope: CompilationContext) {
    return ConstantFP.get(this.builder.getDoubleTy(), node.value);
  }

  addition(node: AdditionNode, scope: CompilationContext) {
    const left = this.visit(node.left, scope);
    const right = this.visit(node.right, scope);

    const builder = this.typeFactory.getIRBuilderForType(node.left.typeInfo);
    return builder.add(left, right);
  }

  subtraction(node: SubtractionNode, scope: CompilationContext) {
    const left = this.visit(node.left, scope);
    const right = this.visit(node.right, scope);
    const builder = this.typeFactory.getIRBuilderForType(node.left.typeInfo);
    return builder.sub(left, right);
  }
  multiplication(node: MultiplicationNode, scope: CompilationContext) {
    const left = this.visit(node.left, scope);
    const right = this.visit(node.right, scope);
    const builder = this.typeFactory.getIRBuilderForType(node.left.typeInfo);
    return builder.mul(left, right);
  }
  division(node: DivisionNode, scope: CompilationContext) {
    const left = this.visit(node.left, scope);
    const right = this.visit(node.right, scope);
    const builder = this.typeFactory.getIRBuilderForType(node.left.typeInfo);
    return builder.div(left, right);
  }

  boolean(node: BooleanNode, scope: CompilationContext) {
    return node.value ? this.builder.getTrue() : this.builder.getFalse();
  }

  visit<T extends BaseNode>(node: T, scope: CompilationContext) {
    if (!node) {
      return null;
    }
    if (!this[node.name]) {
      throw new ExecutionError(
        `Unknown AST node type: ${node.name}`,
        this.compilationLog,
      );
    }
    const result = this[node.name](node as any, scope);
    this.compilationLog.push({ node, scope, result });
    return result;
  }

  program = (node: BaseNode, scope: CompilationContext): any => {
    const mainReturnType = this.builder.getInt64Ty();
    const mainParamTypes = [];
    const mainFunctionType = FunctionType.get(
      mainReturnType,
      mainParamTypes,
      false,
    );
    const mainFunc = Function.Create(
      mainFunctionType,
      Function.LinkageTypes.ExternalLinkage,
      "main",
      this.module,
    );
    const mainEntryBB = BasicBlock.Create(this.context, "main", mainFunc);
    this.builder.SetInsertPoint(mainEntryBB);

    const left = ConstantInt.get(this.builder.getInt64Ty(), 12);
    const right = ConstantInt.get(this.builder.getInt64Ty(), 12);
    const r = this.builder.CreateCall(func, [left, right]);
    this.builder.CreateRet(r);

    if (verifyFunction(func)) {
      console.error("Verifying function failed");
      return this.module;
    }
    if (verifyModule(this.module)) {
      console.error("Verifying module failed");
      return this.module;
    }

    return this.module;
  };
}

export function compileSyntaxTree(
  node: ProgramNode,
  globalContext = new CompilationContext(),
): any {
  const executor = new CompilerVisitor();
  try {
    return executor.visit(node, globalContext);
  } catch (e) {
    if (e instanceof ExecutionError) {
      console.error(e);
    }
    throw e;
  }
}

export function compileProgram(program: string) {
  const syntaxTree = parseProgram(program);
  checkTypes(syntaxTree);
  console.log(syntaxTree);
  const module = compileSyntaxTree(syntaxTree);
  console.log(module);

  // const target = TargetRegistry.lookupTarget("x86_64-unknown-linux-gnu");
  // const targetMachine = target.createTargetMachine(
  //   "unknown-linux-gnu",
  //   "x86_64",
  // );
  // module.setDataLayout(targetMachine.createDataLayout());
  WriteBitcodeToFile(module, "program.bc");

  return module;
}
